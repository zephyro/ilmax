<!doctype html>
<html lang="rus">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->



    <body id="top">
        <div class="wrap" style="background-image: url('img/main-back.png');">

            <div class="wrap-content">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/nav.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-3 sidebar">

                                <!-- Sidenav -->
                                <?php include('inc/sidenav.inc.php') ?>
                                <!-- -->

                                <div class="rubric">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Статьи</div>
                                            <a href="#">Все статьи</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric2.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Правильное утепление крыши в деревянном доме</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="rubric-after">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Совет</div>
                                            <a href="#">Все советы</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-9 main-content">

                                <!-- Main Slider -->
                                <div class="main-slider">
                                    <div class="single-item">
                                        <div>
                                            <div class="slider-page" >
                                                <img src="img/shpat.jpg" alt="">
                                                <div class="slider-content" >
                                                    <h1>Шпатлевка <br><span>гипсовая финишная</span></h1>
                                                    <a href="#" class="more">читать подробнее</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="slider-page" >
                                                <img src="img/shpat.jpg" alt="">
                                                <div class="slider-content" >
                                                    <h1>Шпатлевка <br><span>гипсовая финишная</span></h1>
                                                    <a href="#" class="more">читать подробнее</a>
                                                </div>
                                           </div>
                                        </div>
                                        <div>
                                            <div class="slider-page" >
                                                <img src="img/shpat.jpg" alt="">
                                                <div class="slider-content" >
                                                    <h1>Шпатлевка <br><span>гипсовая финишная</span></h1>
                                                    <a href="#" class="more">читать подробнее</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="next"><i class="fas fa-angle-right"></i></button>
                                    <button class="prev"><i class="fas fa-angle-left"></i></button>
                                </div>
                                <!-- -->

                                <!-- Main Advantages -->
                                <div class="advantages">
                                    <div class="row">
                                        <div class="col-md-4 adv-item">
                                            <div class="adv-square">
                                                <p>26<br><span>лет</span></p>
                                            </div>
                                            <div class="adv-text">
                                                <p>Работаем с 1992 года</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 adv-item">
                                            <div class="adv-square">
                                                <img src="img/quality.png" alt="">
                                            </div>
                                            <div class="adv-text">
                                                <p>Продукция соответствует требованиям ISO 9001</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 adv-item">
                                            <div class="adv-square">
                                                <img src="img/shopping-list.png" alt="">
                                            </div>
                                            <div class="adv-text">
                                                <p>Самый широкий ассортимент</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                                <!-- Main Works -->
                                <div class="works">
                                    <div class="row">
                                        <div class="col-md-6 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Гидро - и пароизоляция</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/gidro-paro.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Облицовка</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/oblicovka.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Кладка</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/kladka.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Выравнивание полов</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/poly.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Ремонт желфезобетонных конструкций</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/zhelezo-beton.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Отделка фасада без утеплителя</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/fasad.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Защита от радиации</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/radioactive.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Облицовка</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/oblicovka.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Кладка</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/kladka.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-6">
                                            <div class="works-type">
                                                <a href="#" class="works-type-shadow"></a>
                                                <div class="works-type-name">
                                                    <a href="#">Отделка фасадов без утеплителя</a>
                                                </div>
                                                <a href="#">
                                                    <img src="img/works/fasad-big.jpg" class="works-photo" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                                <!-- For Someone -->
                                <div class="section-head">
                                    <div class="h3">Для кого</div>
                                </div>
                                <div class="for-whom">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="for-whom-item">
                                                <div class="for-whom-content">
                                                    <div class="h4">Физическим лицам</div>
                                                    <a href="#">Калькулятор расхода</a>
                                                    <a href="#">Советы по ремонту</a>
                                                    <a href="#">Где купить</a>
                                                    <a href="#">Акции</a>
                                                    <a href="#">Задать вопрос специалисту </a>
                                                    <a href="#">Новости</a>
                                                </div>
                                                <img src="img/for-whom/fiz.jpg" class="for-whom-logo" alt="">
                                                <div class="for-whom-gradient"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="for-whom-item">
                                                <div class="for-whom-content">
                                                    <div class="h4">Оптовым клиентам</div>
                                                    <a href="#">Калькулятор расхода</a>
                                                    <a href="#">Советы по ремонту</a>
                                                    <a href="#">Где купить</a>
                                                    <a href="#">Акции</a>
                                                    <a href="#">Задать вопрос специалисту </a>
                                                    <a href="#">Новости</a>
                                                </div>
                                                <img src="img/for-whom/opt.jpg" class="for-whom-logo" alt="">
                                                <div class="for-whom-gradient"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="for-whom-item">
                                                <div class="for-whom-content">
                                                    <div class="h4">Строительным и  проектным организациям</div>
                                                    <a href="#">Калькулятор расхода</a>
                                                    <a href="#">Советы по ремонту</a>
                                                    <a href="#">Где купить</a>
                                                    <a href="#">Акции</a>
                                                    <a href="#">Задать вопрос специалисту </a>
                                                    <a href="#">Новости</a>
                                                </div>
                                                <img src="img/for-whom/org.jpg" class="for-whom-logo" alt="">
                                                <div class="for-whom-gradient"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                                <!-- News -->
                                <div class="news">
                                    <div class="section-head">
                                        <div class="h3">Новости
                                            <span><a href="#">Все новости</a></span>
                                        </div>
                                        <div class="section-control">
                                            <button class="sect-contr-prev prev-news"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-news"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="slider-wrapper">
                                        <div class="news-slider">
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news1.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news2.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news3.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news1.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news2.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news3.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                          </div>
                                          <div class="section-control section-control-mobile">
                                              <button class="sect-contr-prev prev-news"><i class="fas fa-chevron-left"></i></button>
                                              <button class="sect-contr-next next-news"><i class="fas fa-chevron-right"></i></button>
                                          </div>
                                      </div>
                                </div>
                                <!-- -->

                                <!-- Subscribe -->
                                <div class="subscribe" style="background-image: url('img/subscribe-back.png');">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="subscribe-text ">
                                                <div class="h5">Подписывайтесь</div>
                                                <p>Узнайте свежую информацию об акциях и скидках первым!</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7 align-self-center">
                                            <div class="subscribe-form">
                                                <div class="row">
                                                    <input type="text" class="subscribe-form-input" placeholder="Ваш e-mail">
                                                    <button  class="subscribe-form-btn">Подписаться</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                                <!-- Videos -->
                                <div class="videos">
                                    <div class="section-head">
                                        <div class="h3">Видео
                                            <span><a href="#">Все видео</a></span>
                                        </div>
                                        <div class="section-control">
                                            <button class="sect-contr-prev prev-video"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-video"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>

                                    <div class="slider-wrapper">
                                        <div class="video-slider">
                                            <div>
                                                <div class="video-item">
                                                    <img src="img/video-logo.png" class="logo-video" alt="">
                                                    <a data-fancybox href="https://www.youtube.com/embed/VVO1UpNW5lY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="video-link" data-fancybox="group">
                                                        <span class="video-link-name">Клей для плитки ilmax 3000</span>
                                                        <span class="video-link-btn"><img src="img/play.png" alt=""></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="video-item">
                                                    <img src="img/video-logo.png" class="logo-video" alt="">
                                                    <a data-fancybox href="https://www.youtube.com/embed/VVO1UpNW5lY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="video-link" data-fancybox="group">
                                                        <span class="video-link-name">Клей для плитки ilmax 3000</span>
                                                        <span class="video-link-btn"><img src="img/play.png" alt=""></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="video-item">
                                                    <img src="img/video-logo.png" class="logo-video" alt="">
                                                    <a data-fancybox href="https://www.youtube.com/embed/VVO1UpNW5lY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="video-link" data-fancybox="group">
                                                        <span class="video-link-name">Клей для плитки ilmax 3000</span>
                                                        <span class="video-link-btn"><img src="img/play.png" alt=""></span>
                                                    </a>
                                                  </div>
                                            </div>
                                            <div>
                                                <div class="video-item">
                                                    <img src="img/video-logo.png" class="logo-video" alt="">
                                                    <a data-fancybox href="https://www.youtube.com/embed/VVO1UpNW5lY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="video-link" data-fancybox="group">
                                                        <span class="video-link-name">Клей для плитки ilmax 3000</span>
                                                        <span class="video-link-btn"><img src="img/play.png" alt=""></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="video-item">
                                                    <img src="img/video-logo.png" class="logo-video" alt="">
                                                    <a data-fancybox href="https://www.youtube.com/embed/VVO1UpNW5lY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="video-link" data-fancybox="group">
                                                        <span class="video-link-name">Клей для плитки ilmax 3000</span>
                                                        <span class="video-link-btn"><img src="img/play.png" alt=""></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="video-item">
                                                    <img src="img/video-logo.png" class="logo-video" alt="">
                                                    <a data-fancybox href="https://www.youtube.com/embed/VVO1UpNW5lY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="video-link" data-fancybox="group">
                                                        <span class="video-link-name">Клей для плитки ilmax 3000</span>
                                                        <span class="video-link-btn"><img src="img/play.png" alt=""></span>
                                                    </a>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="video-item">
                                                    <img src="img/video-logo.png" class="logo-video" alt="">
                                                    <a data-fancybox href="https://www.youtube.com/embed/VVO1UpNW5lY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" class="video-link" data-fancybox="group">
                                                        <span class="video-link-name">Клей для плитки ilmax 3000</span>
                                                        <span class="video-link-btn"><img src="img/play.png" alt=""></span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="section-control section-control-mobile">
                                            <button class="sect-contr-prev prev-video"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-video"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                                <!-- Новинки -->
                                <div class="new-ones">
                                    <div class="section-head">
                                        <div class="h3">Новинки
                                            <span><a href="#">Все новинки</a></span>
                                        </div>
                                        <div class="section-control">
                                            <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="slider-wrapper">
                                        <div class="slider_border new-ones-slider">
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="section-control section-control-mobile">
                                            <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->
                            </div>

                        </div>
                    </div>

                    <!-- Feedback -->
                    <?php include('inc/feedback.inc.php') ?>
                    <!-- -->

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

      </body>

</html>
