<!doctype html>
<html lang="rus">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body id="top">
        <div class="wrap">

            <div class="wrap-content">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/nav.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-3 sidebar">

                                <!-- Sidenav -->
                                <?php include('inc/sidenav.inc.php') ?>
                                <!-- -->

                                <div class="rubric">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Статьи</div>
                                            <a href="#">Все статьи</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric2.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Правильное утепление крыши в деревянном доме</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="rubric-after">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Совет</div>
                                            <a href="#">Все советы</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-9 main-content">

                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><span> Сравнение товаров</span></li>
                                </ul>

                                <h1 class="main_heading">Сравнение товаров</h1>

                                <div class="compare__clear_wrap">
                                    <a href="#" class="compare__clear"><span>Очистить список сравнения</span></a>
                                </div>

                                <div class="compare">

                                    <div class="compare__scroll">


                                        <div class="compare__wrap">

                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend compare__elem_heading">

                                                </div>

                                                <div class="compare__elem compare__elem_data compare__elem_heading">
                                                    <div class="compare_product">
                                                        <div class="compare_product__remove"></div>
                                                        <a class="compare_product__image" href="#">
                                                            <img src="images/compare__img.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <a class="compare_product__name" href="#" >Клей для плитки ilmax 3000</a>
                                                        <div class="compare_product__type">Клеи для плитки</div>
                                                    </div>
                                                </div>
                                                <div class="compare__elem compare__elem_data compare__elem_heading">
                                                    <div class="compare_product">
                                                        <div class="compare_product__remove"></div>
                                                        <a class="compare_product__image" href="#">
                                                            <img src="images/compare__img.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <a class="compare_product__name" href="#" >Клей для плитки ilmax 3000</a>
                                                        <div class="compare_product__type">Клеи для плитки</div>
                                                    </div>
                                                </div>
                                                <div class="compare__elem compare__elem_data compare__elem_heading">
                                                    <div class="compare_product">
                                                        <div class="compare_product__remove"></div>
                                                        <a class="compare_product__image" href="#">
                                                            <img src="images/compare__img.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <a class="compare_product__name" href="#" >Клей для плитки ilmax 3000</a>
                                                        <div class="compare_product__type">Клеи для плитки</div>
                                                    </div>
                                                </div>
                                                <div class="compare__elem compare__elem_data compare__elem_heading">
                                                    <div class="compare_product">
                                                        <div class="compare_product__remove"></div>
                                                        <a class="compare_product__image" href="#">
                                                            <img src="images/compare__img.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <a class="compare_product__name" href="#" >Клей для плитки ilmax 3000</a>
                                                        <div class="compare_product__type">Клеи для плитки</div>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="compare__nav">

                                            </div>

                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Цвет</span></div>
                                                <div class="compare__elem compare__elem_data">серый</div>
                                                <div class="compare__elem compare__elem_data">серый</div>
                                                <div class="compare__elem compare__elem_data">серый</div>
                                                <div class="compare__elem compare__elem_data">серый</div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Время использования раствора, не более</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,5 ч</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,5 ч</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,5 ч</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,5 ч</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Расход</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,6 кг/м2*1мм</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,6 кг/м2*1мм</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,6 кг/м2*1мм</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,6 кг/м2*1мм</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Дальнейшие отделочные работы, не ранее</span></div>
                                                <div class="compare__elem compare__elem_data"><span>3 суток</span></div>
                                                <div class="compare__elem compare__elem_data"><span>3 суток</span></div>
                                                <div class="compare__elem compare__elem_data"><span>3 суток</span></div>
                                                <div class="compare__elem compare__elem_data"><span>3 суток</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Пригодна к эксплуатации, не ранее</span></div>
                                                <div class="compare__elem compare__elem_data"><span>7 суток</span></div>
                                                <div class="compare__elem compare__elem_data"><span>7 суток</span></div>
                                                <div class="compare__elem compare__elem_data"><span>7 суток</span></div>
                                                <div class="compare__elem compare__elem_data"><span>7 суток</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Адгезия, не менее</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,2 МПа</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,2 МПа</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,2 МПа</span></div>
                                                <div class="compare__elem compare__elem_data"><span>1,2 МПа</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Водонепроницаемость, не менее</span></div>
                                                <div class="compare__elem compare__elem_data"><span>8 атм</span></div>
                                                <div class="compare__elem compare__elem_data"><span>8 атм</span></div>
                                                <div class="compare__elem compare__elem_data"><span>8 атм</span></div>
                                                <div class="compare__elem compare__elem_data"><span>8 атм</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Водопоглощение при капилярном подсосе, не более</span></div>
                                                <div class="compare__elem compare__elem_data"><span>0,2 кг/м2</span></div>
                                                <div class="compare__elem compare__elem_data"><span>0,2 кг/м2</span></div>
                                                <div class="compare__elem compare__elem_data"><span>0,2 кг/м2</span></div>
                                                <div class="compare__elem compare__elem_data"><span>0,2 кг/м2</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Морозостойкость, не менее</span></div>
                                                <div class="compare__elem compare__elem_data"><span>75 циклов</span></div>
                                                <div class="compare__elem compare__elem_data"><span>75 циклов</span></div>
                                                <div class="compare__elem compare__elem_data"><span>75 циклов</span></div>
                                                <div class="compare__elem compare__elem_data"><span>75 циклов</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Температура проведения работ</span></div>
                                                <div class="compare__elem compare__elem_data"><span>+5°С...+25°С</span></div>
                                                <div class="compare__elem compare__elem_data"><span>+5°С...+25°С</span></div>
                                                <div class="compare__elem compare__elem_data"><span>+5°С...+25°С</span></div>
                                                <div class="compare__elem compare__elem_data"><span>+5°С...+25°С</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Температура эксплуатации</span></div>
                                                <div class="compare__elem compare__elem_data"><span>-30°С...+70°С</span></div>
                                                <div class="compare__elem compare__elem_data"><span>-30°С...+70°С</span></div>
                                                <div class="compare__elem compare__elem_data"><span>-30°С...+70°С</span></div>
                                                <div class="compare__elem compare__elem_data"><span>-30°С...+70°С</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Срок хранения</span></div>
                                                <div class="compare__elem compare__elem_data"><span>12 месяцев</span></div>
                                                <div class="compare__elem compare__elem_data"><span>12 месяцев</span></div>
                                                <div class="compare__elem compare__elem_data"><span>12 месяцев</span></div>
                                                <div class="compare__elem compare__elem_data"><span>12 месяцев</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Фасовка</span></div>
                                                <div class="compare__elem compare__elem_data"><span>Компонент Б 8 кг (8 л)</span></div>
                                                <div class="compare__elem compare__elem_data"><span>Компонент Б 8 кг (8 л)</span></div>
                                                <div class="compare__elem compare__elem_data"><span>Компонент Б 8 кг (8 л)</span></div>
                                                <div class="compare__elem compare__elem_data"><span>Компонент Б 8 кг (8 л)</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Количество единиц на поддоне</span></div>
                                                <div class="compare__elem compare__elem_data"><span>40 шт.</span></div>
                                                <div class="compare__elem compare__elem_data"><span>40 шт.</span></div>
                                                <div class="compare__elem compare__elem_data"><span>40 шт.</span></div>
                                                <div class="compare__elem compare__elem_data"><span>40 шт.</span></div>
                                            </div>
                                            <div class="compare__row">
                                                <div class="compare__elem compare__elem_legend"><span>Вес поддона</span></div>
                                                <div class="compare__elem compare__elem_data"><span>320 кг</span></div>
                                                <div class="compare__elem compare__elem_data"><span>320 кг</span></div>
                                                <div class="compare__elem compare__elem_data"><span>320 кг</span></div>
                                                <div class="compare__elem compare__elem_data"><span>320 кг</span></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="compare__legend">
                                        <ul>
                                            <li>Цвет</li>
                                            <li>Время использования раствора, не более</li>
                                            <li>Расход</li>
                                            <li>Дальнейшие отделочные работы, не ранее</li>
                                            <li>Пригодна к эксплуатации, не ранее</li>
                                            <li>Адгезия, не менее</li>
                                            <li>Водонепроницаемость, не менее</li>
                                            <li>Водопоглощение при капилярном подсосе, не более</li>
                                            <li>Морозостойкость, не менее</li>
                                            <li>Температура проведения работ</li>
                                            <li>Температура эксплуатации</li>
                                            <li>Срок хранения</li>
                                            <li>Фасовка</li>
                                            <li>Количество единиц на поддоне</li>
                                            <li>Вес поддона</li>
                                        </ul>
                                    </div>

                                </div>


                            </div>

                        </div>
                    </div>

                    <!-- Feedback -->
                    <?php include('inc/feedback.inc.php') ?>
                    <!-- -->

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

      </body>

</html>
