<!doctype html>
<html lang="rus">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body id="top">
        <div class="wrap">

            <div class="wrap-content">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/nav.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-3 sidebar">

                                <!-- Sidenav -->
                                <?php include('inc/sidenav.inc.php') ?>
                                <!-- -->

                                <div class="rubric">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Статьи</div>
                                            <a href="#">Все статьи</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric2.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Правильное утепление крыши в деревянном доме</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="rubric-after">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Совет</div>
                                            <a href="#">Все советы</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-9 main-content">

                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><a href="#">Каталог</a></li>
                                    <li><span>Фуги для плитки</span></li>
                                </ul>

                                <h1 class="main_heading">Фуги для плитки</h1>

                                <ul class="options">

                                    <li>
                                        <div class="select_main select_single">
                                            <div class="select_single__label">
                                                <span>Тип облицовочного материала</span>
                                                <i class="fas fa-chevron-down"></i>
                                            </div>
                                            <div class="select_single__dropdown">
                                                <label class="select_single__item">
                                                    <input type="radio" name="m_type" value="Тип облицовочного материала">
                                                    <span>Тип облицовочного материала</span>
                                                </label>
                                                <label class="select_single__item">
                                                    <input type="radio" name="m_type" value="керамическая плитка и керамическая мозаика">
                                                    <span>керамическая плитка и керамическая мозаика</span>
                                                </label>
                                                <label class="select_single__item">
                                                    <input type="radio" name="m_type" value="ГРЕС и клинкерная плитка">
                                                    <span>ГРЕС и клинкерная плитка</span>
                                                </label>
                                                <label class="select_single__item">
                                                    <input type="radio" name="m_type" value="Натуральный и искусственный камень">
                                                    <span>Натуральный и искусственный камень</span>
                                                </label>
                                                <label class="select_single__item">
                                                    <input type="radio" name="m_type" value="Мраморная и стеклянная мозаика">
                                                    <span>Мраморная и стеклянная мозаика</span>
                                                </label>
                                                <label class="select_single__item">
                                                    <input type="radio" name="m_type" value="Кислотоупорная плитка">
                                                    <span>Кислотоупорная плитка</span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="select_main select_multiple">
                                            <div class="select_multiple__label">
                                                <span class="select_multiple__label_text">Цвет</span>
                                                <span class="select_multiple__label_value">2</span>
                                                <i class="fas fa-chevron-down"></i>
                                            </div>
                                            <div class="select_multiple__dropdown">
                                                <span class="select_multiple__close"></span>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="Белый" checked>
                                                    <span>Белый</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="Синий" checked>
                                                    <span>Синий</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="Красный">
                                                    <span>Красный</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="Желтый">
                                                    <span>Желтый</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="бежевый">
                                                    <span>бежевый</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="Черный">
                                                    <span>Черный</span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="select_main select_multiple">
                                            <div class="select_multiple__label">
                                                <span class="select_multiple__label_text">Область применения</span>
                                                <span class="select_multiple__label_value">0</span>
                                                <i class="fas fa-chevron-down"></i>
                                            </div>
                                            <div class="select_multiple__dropdown">
                                                <span class="select_multiple__close"></span>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="вертикальное основание">
                                                    <span>вертикальное основание</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="горизонтальное основание">
                                                    <span>горизонтальное основание</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="внутренние работы">
                                                    <span>внутренние работы</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="наружные работы">
                                                    <span>наружные работы</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="интенсивные нагрузки">
                                                    <span>интенсивные нагрузки</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="длительное воздействие влаги">
                                                    <span>длительное воздействие влаги</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="">
                                                    <span>воздействие высоких температур</span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="select_main select_multiple">
                                            <div class="select_multiple__label">
                                                <span class="select_multiple__label_text">Прочность</span>
                                                <span class="select_multiple__label_value">0</span>
                                                <i class="fas fa-chevron-down"></i>
                                            </div>
                                            <div class="select_multiple__dropdown">
                                                <span class="select_multiple__close"></span>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="">
                                                    <span>Хрупкий</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="">
                                                    <span>Нормальный</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="">
                                                    <span>Прочный</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="">
                                                    <span>Очень прочный</span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="select_main select_multiple">
                                            <div class="select_multiple__label">
                                                <span class="select_multiple__label_text">Температура</span>
                                                <span class="select_multiple__label_value">0</span>
                                                <i class="fas fa-chevron-down"></i>
                                            </div>
                                            <div class="select_multiple__dropdown">
                                                <span class="select_multiple__close"></span>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="">
                                                    <span>До 30 градусов</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="">
                                                    <span>От 30 до 60 градусов</span>
                                                </label>
                                                <label class="select_multiple__item">
                                                    <input type="checkbox" name="m_check" value="">
                                                    <span>Свыше 60 градусов</span>
                                                </label>
                                            </div>
                                        </div>
                                    </li>

                                </ul>

                                <div class="main_filter">
                                    <div class="main_filter__show">
                                        <span class="main_filter__legend">Показывать по</span>
                                        <div class="main_filter__select">
                                            <select class="form_select">
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="50">20</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                        <span class="main_filter__legend hide-xs">товаров</span>
                                    </div>
                                    <div class="main_filter__view">
                                        <a href="#" class="active">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 21 21" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__tile" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                        <a href="#">
                                            <i>
                                                <svg class="ico-svg" viewBox="0 0 20 12" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__list" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                    </div>

                                </div>

                                <div class="showcase">

                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="showcase__col">
                                        <div class="goods">
                                            <div class="goods__image">
                                                <img src="img/new-ones.png" class="img-fluid">
                                            </div>
                                            <div class="goods__content">

                                                <div class="goods__data">
                                                    <div class="goods__data_name">
                                                        <span>ilmax 2000</span>
                                                        <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                    </div>
                                                    <div class="goods__data_type">
                                                        МИНЕРАЛЬНАЯ
                                                        <br/>
                                                        ВЫРАВНИВАЮЩАЯ
                                                    </div>
                                                    <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                    <ul class="goods__data_info">
                                                        <li>Подходит для пола и стен</li>
                                                        <li>Для керамической плитки</li>
                                                        <li>Для плитки размером до 40х40 см</li>
                                                        <li>Подходит для влажных работ</li>
                                                    </ul>
                                                </div>

                                                <div class="goods__action">
                                                    <a href="#" class="goods__button goods__button_compare">
                                                        <span>Сравнить</span>
                                                    </a>
                                                    <a href="#" class="goods__button goods__button_view">
                                                        <span>Подробнее</span>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="pag">
                                    <ul class="main_pagination">
                                        <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                                        <li class="active"><span>1</span></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                                    </ul>
                                </div>

                                <div class="h3">Информационный блок</div>
                                <div class="info_block">
                                    <div class="row">
                                        <div class="col-md-6  info_block__logo">
                                            <img src="img/info-block-logo.png" class="info_block__logo_image" alt="">
                                        </div>
                                        <div class="col-md-6">
                                            <p>
                                                Рыбным текстом называется текст, служащий для временного наполнения макета в публикациях или производстве веб-сайтов, пока финальный текст еще не создан.
                                            </p>
                                            <p>
                                                Рыбный текст также известен как текст-заполнитель или же текст-наполнитель. Иногда текст-«рыба» также используется композиторами при написании музыки.
                                            </p>
                                            <p>
                                                Они напевают его перед тем, как сочинены соответствующие слова.
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <!-- Feedback -->
                    <?php include('inc/feedback.inc.php') ?>
                    <!-- -->

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

      </body>

</html>
