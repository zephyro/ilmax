<!doctype html>
<html lang="rus">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body id="top">
        <div class="wrap">

            <div class="wrap-content">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/nav.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-3 sidebar">

                                <div class="contact_nav">
                                    <div class="contact_nav__title">Беларусь</div>
                                    <ul>
                                        <li class="active"><a href="#">Минская область</a></li>
                                        <li><a href="#">Витебская область</a></li>
                                        <li><a href="#">Гродненская область</a></li>

                                    </ul>
                                    <div class="contact_nav__title">Россия</div>
                                    <ul>
                                        <li><a href="#">Московская область</a></li>
                                    </ul>
                                </div>


                                <div class="rubric">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Статьи</div>
                                            <a href="#">Все статьи</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric2.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Правильное утепление крыши в деревянном доме</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="rubric-after">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Совет</div>
                                            <a href="#">Все советы</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-9 main-content">

                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><span>Где купить</span></li>
                                </ul>

                                <ul class="contact_type">
                                   <li>
                                       <a href="#">
                                           <i>
                                               <img src="img/icon__contact_01.png" class="img-fluid" alt="">
                                           </i>
                                           <span>официальные дилеры</span>
                                       </a>
                                   </li>
                                    <li>
                                        <a href="#">
                                            <i>
                                                <img src="img/icon__contact_02.png" class="img-fluid" alt="">
                                            </i>
                                            <span>оптовые компании</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i>
                                                <img src="img/icon__contact_03.png" class="img-fluid" alt="">
                                            </i>
                                            <span>розничные магазины</span>
                                        </a>
                                    </li>
                                </ul>

                                <div class="contact_map" id="map"></div>

                                <div class="contact_group">
                                    <h2>Официальные дилеры в Минской области</h2>

                                    <div class="contact active">
                                        <div class="contact__info">
                                            <a class="contact__info_name" href="#">Название компании</a>
                                            <div class="contact__info_address">г. Минск, ул. Большая, 7/10</div>
                                            <div class="contact__info_metro">Охотный ряд</div>
                                        </div>
                                        <div class="contact__phones">
                                            <ul>
                                                <li><a href="tel:+7 (800) 200 00 00">+7 (800) 200 00 00</a></li>
                                                <li><a href="tel:+7 (449) 334 56 92">+7 (449) 334 56 92</a></li>
                                                <li><a href="tel:+7 (448) 852 02 02">+7 (448) 852 02 02</a></li>

                                            </ul>
                                        </div>
                                        <div class="contact__email">
                                            <ul>
                                                <li><a href="mailto:info@site.ru">info@site.ru</a></li>
                                                <li><a href="mailto:first-site.ru">first-site.ru</a></li>
                                                <li><a href="mailto:second-site.ru">second-site.ru</a></li>

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="contact">
                                        <div class="contact__info">
                                            <a class="contact__info_name" href="#">Название компании</a>
                                            <div class="contact__info_address">г. Минск, ул. Большая, 7/10</div>
                                            <div class="contact__info_metro">Охотный ряд</div>
                                        </div>
                                        <div class="contact__phones">
                                            <ul>
                                                <li><a href="tel:+7 (800) 200 00 00">+7 (800) 200 00 00</a></li>
                                                <li><a href="tel:+7 (449) 334 56 92">+7 (449) 334 56 92</a></li>
                                                <li><a href="tel:+7 (448) 852 02 02">+7 (448) 852 02 02</a></li>

                                            </ul>
                                        </div>
                                        <div class="contact__email">
                                            <ul>
                                                <li><a href="mailto:info@site.ru">info@site.ru</a></li>
                                                <li><a href="mailto:first-site.ru">first-site.ru</a></li>
                                                <li><a href="mailto:second-site.ru">second-site.ru</a></li>

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="contact">
                                        <div class="contact__info">
                                            <a class="contact__info_name" href="#">Название компании</a>
                                            <div class="contact__info_address">г. Минск, ул. Большая, 7/10</div>
                                            <div class="contact__info_metro">Охотный ряд</div>
                                        </div>
                                        <div class="contact__phones">
                                            <ul>
                                                <li><a href="tel:+7 (800) 200 00 00">+7 (800) 200 00 00</a></li>
                                                <li><a href="tel:+7 (449) 334 56 92">+7 (449) 334 56 92</a></li>
                                                <li><a href="tel:+7 (448) 852 02 02">+7 (448) 852 02 02</a></li>

                                            </ul>
                                        </div>
                                        <div class="contact__email">
                                            <ul>
                                                <li><a href="mailto:info@site.ru">info@site.ru</a></li>
                                                <li><a href="mailto:first-site.ru">first-site.ru</a></li>
                                                <li><a href="mailto:second-site.ru">second-site.ru</a></li>

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="contact">
                                        <div class="contact__info">
                                            <a class="contact__info_name" href="#">Название компании</a>
                                            <div class="contact__info_address">г. Минск, ул. Большая, 7/10</div>
                                            <div class="contact__info_metro">Охотный ряд</div>
                                        </div>
                                        <div class="contact__phones">
                                            <ul>
                                                <li><a href="tel:+7 (800) 200 00 00">+7 (800) 200 00 00</a></li>
                                                <li><a href="tel:+7 (449) 334 56 92">+7 (449) 334 56 92</a></li>
                                                <li><a href="tel:+7 (448) 852 02 02">+7 (448) 852 02 02</a></li>

                                            </ul>
                                        </div>
                                        <div class="contact__email">
                                            <ul>
                                                <li><a href="mailto:info@site.ru">info@site.ru</a></li>
                                                <li><a href="mailto:first-site.ru">first-site.ru</a></li>
                                                <li><a href="mailto:second-site.ru">second-site.ru</a></li>

                                            </ul>
                                        </div>
                                    </div>

                                </div>


                            </div>

                        </div>
                    </div>

                    <!-- Feedback -->
                    <?php include('inc/feedback.inc.php') ?>
                    <!-- -->

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

        <script src="js/map.js" type="text/javascript"></script>

      </body>

</html>
