<!doctype html>
<html lang="rus">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body id="top">
        <div class="wrap">

            <div class="wrap-content">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/nav.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-3 sidebar">

                                <ul class="second_nav">
                                    <li><a href="#">О компании</a></li>
                                    <li><a href="#">Новости</a></li>
                                    <li><a href="#">Вакансии</a></li>
                                    <li><a href="#">Сотрудничество</a></li>
                                    <li><a href="#">отзывы</a></li>
                                    <li class="active"><a href="#">награды</a></li>
                                </ul>


                                <div class="rubric">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Статьи</div>
                                            <a href="#">Все статьи</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric2.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Правильное утепление крыши в деревянном доме</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="rubric-after">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Совет</div>
                                            <a href="#">Все советы</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-9 main-content">

                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><span>Награды</span></li>
                                </ul>

                                <h1>Награды</h1>

                                <div class="reward__row">
                                    <div class="reward">
                                        <a class="reward__image" href="images/cert/cert__01.jpg" data-fancybox="cert">
                                            <img src="images/cert/cert__01.jpg" class="img-fluid" alt="">
                                        </a>
                                        <div class="reward__text">Разработка интерьера на высоком уровне требует вливания значительных временных ресурсов.</div>
                                    </div>
                                    <div class="reward">
                                        <a class="reward__image" href="images/cert/cert__02.jpg" data-fancybox="cert">
                                            <img src="images/cert/cert__02.jpg" class="img-fluid" alt="">
                                        </a>
                                        <div class="reward__text">Разработка интерьера на высоком уровне требует вливания значительных временных ресурсов.</div>
                                    </div>
                                    <div class="reward">
                                        <a class="reward__image" href="images/cert/cert__03.jpg" data-fancybox="cert">
                                            <img src="images/cert/cert__03.jpg" class="img-fluid" alt="">
                                        </a>
                                        <div class="reward__text">Разработка интерьера на высоком уровне требует вливания значительных временных ресурсов.</div>
                                    </div>
                                    <div class="reward">
                                        <a class="reward__image" href="images/cert/cert__04.jpg" data-fancybox="cert">
                                            <img src="images/cert/cert__04.jpg" class="img-fluid" alt="">
                                        </a>
                                        <div class="reward__text">Разработка интерьера на высоком уровне требует вливания значительных временных ресурсов.</div>
                                    </div>
                                    <div class="reward">
                                        <a class="reward__image" href="images/cert/cert__05.jpg" data-fancybox="cert">
                                            <img src="images/cert/cert__05.jpg" class="img-fluid" alt="">
                                        </a>
                                        <div class="reward__text">Разработка интерьера на высоком уровне требует вливания значительных временных ресурсов.</div>
                                    </div>
                                    <div class="reward">
                                        <a class="reward__image" href="images/cert/cert__06.jpg" data-fancybox="cert">
                                            <img src="images/cert/cert__06.jpg" class="img-fluid" alt="">
                                        </a>
                                        <div class="reward__text">Разработка интерьера на высоком уровне требует вливания значительных временных ресурсов.</div>
                                    </div>
                                </div>

                                <!-- Subscribe -->
                                <div class="subscribe" style="background-image: url('img/subscribe-back.png');">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="subscribe-text ">
                                                <div class="h5">Подписывайтесь</div>
                                                <p>Узнайте свежую информацию об акциях и скидках первым!</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7 align-self-center">
                                            <div class="subscribe-form">
                                                <div class="row">
                                                    <input type="text" class="subscribe-form-input" placeholder="Ваш e-mail">
                                                    <button  class="subscribe-form-btn">Подписаться</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->
                                <!-- Новинки -->
                                <div class="new-ones">
                                    <div class="section-head">
                                        <div class="h3">Новинки
                                            <span><a href="#">Все новинки</a></span>
                                        </div>
                                        <div class="section-control">
                                            <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="slider-wrapper">
                                        <div class="slider_border new-ones-slider">
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="section-control section-control-mobile">
                                            <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                            </div>

                        </div>
                    </div>

                    <!-- Feedback -->
                    <?php include('inc/feedback.inc.php') ?>
                    <!-- -->

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <!-- Modal -->
        <div class="modal" id="response">
            <div class="modal__title">Название вакансии</div>
            <form class="form">
                <div class="form_group">
                    <label class="form_label">ФИО <sup>*</sup></label>
                    <input type="text" class="form_control" name="name" value="" placeholder="">
                </div>
                <div class="form_group">
                    <label class="form_label">Телефон <sup>*</sup></label>
                    <input type="text" class="form_control" name="phone" value="" placeholder="">
                </div>
                <div class="form_group">
                    <label class="form_label">Email <sup>*</sup></label>
                    <input type="text" class="form_control" name="email" value="" placeholder="">
                </div>
                <div class="form_group">
                    <label class="form_label">Комментарии</label>
                    <textarea class="form_control" name="" placeholder="" rows=5"></textarea>
                </div>
                <div class="form_group mb_30">
                    <div class="file_form">
                        <label class="file_form__label">
                            <input type="file" name="file" value="">
                            <i>
                                <svg class="ico-svg" viewBox="0 0 15 18" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__clip" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                            <span>Прикрепить файл</span>
                        </label>
                        <div class="file_form__elem">
                            <span>Название_файла.jpg</span>
                            <i class="file_form__remove"></i>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <button type="submit" class="btn_main">Отправить отзыв</button>
                </div>
            </form>
        </div>
        <!-- -->

      </body>

</html>
