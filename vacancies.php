<!doctype html>
<html lang="rus">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body id="top">
        <div class="wrap">

            <div class="wrap-content">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/nav.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-3 sidebar">

                                <ul class="second_nav">
                                    <li><a href="#">О компании</a></li>
                                    <li><a href="#">Новости</a></li>
                                    <li class="active"><a href="#">Вакансии</a></li>
                                    <li><a href="#">Сотрудничество</a></li>
                                    <li><a href="#">отзывы</a></li>
                                    <li><a href="#">награды</a></li>
                                </ul>


                                <div class="rubric">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Статьи</div>
                                            <a href="#">Все статьи</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric2.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Правильное утепление крыши в деревянном доме</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="rubric-after">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Совет</div>
                                            <a href="#">Все советы</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-9 main-content">

                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><span>Вакансии</span></li>
                                </ul>

                                <h1>Вакансии</h1>

                                <div class="company_box">
                                    <div class="company_box__image">
                                        <div class="company_box__logo">
                                            <img src="images/v_logo.png" class="img-fluid" alt="">
                                        </div>
                                    </div>
                                    <div class="company_box__text">
                                        <p>Рыбным текстом называется текст, служащий для временного наполнения макета в публикациях или производстве веб-сайтов, пока финальный текст еще не создан.</p>
                                        <p>Рыбный текст также известен как текст-заполнитель или же текст-наполнитель. Иногда текст-«рыба» также используется композиторами при написании музыки.</p>
                                        <p>Они напевают его перед тем, как сочинены соответствующие слова.</p>
                                    </div>
                                </div>

                                <div class="vacancies">
                                    <h4>PHP-программист</h4>
                                    <ul class="vacancies__requirement">
                                        <li>Город: Краснодар</li>
                                        <li>Образование: Высшее</li>
                                        <li>Опыт работы: от 2 лет</li>
                                    </ul>
                                    <div class="vacancies__text">
                                        Каким должен быть дизайн интерьера кухни? Прежде всего, комфортабельным, эргономичным, детально продуманным. Разработка интерьера на высоком уровне требует вливания значительных средств
                                        и временных ресурсов. Ремонт начинается с создания дизайн-проекта, нарисованного от руки или
                                    </div>
                                    <a href="#" class="btn_border">откликнуться</a>
                                </div>

                                <div class="vacancies">
                                    <h4>PHP-программист</h4>
                                    <ul class="vacancies__requirement">
                                        <li>Город: Краснодар</li>
                                        <li>Образование: Высшее</li>
                                        <li>Опыт работы: от 2 лет</li>
                                    </ul>
                                    <div class="vacancies__text">
                                        Каким должен быть дизайн интерьера кухни? Прежде всего, комфортабельным, эргономичным, детально продуманным. Разработка интерьера на высоком уровне требует вливания значительных средств
                                        и временных ресурсов. Ремонт начинается с создания дизайн-проекта, нарисованного от руки или
                                    </div>
                                    <a href="#" class="btn_border">откликнуться</a>
                                </div>

                                <div class="vacancies">
                                    <h4>PHP-программист</h4>
                                    <ul class="vacancies__requirement">
                                        <li>Город: Краснодар</li>
                                        <li>Образование: Высшее</li>
                                        <li>Опыт работы: от 2 лет</li>
                                    </ul>
                                    <div class="vacancies__text">
                                        Каким должен быть дизайн интерьера кухни? Прежде всего, комфортабельным, эргономичным, детально продуманным. Разработка интерьера на высоком уровне требует вливания значительных средств
                                        и временных ресурсов. Ремонт начинается с создания дизайн-проекта, нарисованного от руки или
                                    </div>
                                    <a href="#" class="btn_border">откликнуться</a>
                                </div>

                                <ul class="main_pagination pt_20 mb_60">
                                    <li><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
                                    <li class="active"><span>1</span></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
                                </ul>


                                <!-- Subscribe -->
                                <div class="subscribe" style="background-image: url('img/subscribe-back.png');">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="subscribe-text ">
                                                <div class="h5">Подписывайтесь</div>
                                                <p>Узнайте свежую информацию об акциях и скидках первым!</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7 align-self-center">
                                            <div class="subscribe-form">
                                                <div class="row">
                                                    <input type="text" class="subscribe-form-input" placeholder="Ваш e-mail">
                                                    <button  class="subscribe-form-btn">Подписаться</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->
                                <!-- Новинки -->
                                <div class="new-ones">
                                    <div class="section-head">
                                        <div class="h3">Новинки
                                            <span><a href="#">Все новинки</a></span>
                                        </div>
                                        <div class="section-control">
                                            <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="slider-wrapper">
                                        <div class="slider_border new-ones-slider">
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="goods">
                                                    <div class="goods__image">
                                                        <img src="img/new-ones.png" class="img-fluid">
                                                    </div>
                                                    <div class="goods__content">

                                                        <div class="goods__data">
                                                            <div class="goods__data_name">
                                                                <span>ilmax 2000</span>
                                                                <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                            </div>
                                                            <div class="goods__data_type">
                                                                МИНЕРАЛЬНАЯ
                                                                <br/>
                                                                ВЫРАВНИВАЮЩАЯ
                                                            </div>
                                                            <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                            <ul class="goods__data_info">
                                                                <li>Подходит для пола и стен</li>
                                                                <li>Для керамической плитки</li>
                                                                <li>Для плитки размером до 40х40 см</li>
                                                                <li>Подходит для влажных работ</li>
                                                            </ul>
                                                        </div>

                                                        <div class="goods__action">
                                                            <a href="#" class="goods__button goods__button_compare">
                                                                <span>Сравнить</span>
                                                            </a>
                                                            <a href="#" class="goods__button goods__button_view">
                                                                <span>Подробнее</span>
                                                            </a>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="section-control section-control-mobile">
                                            <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                            </div>

                        </div>
                    </div>

                    <!-- Feedback -->
                    <?php include('inc/feedback.inc.php') ?>
                    <!-- -->

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>


      </body>

</html>
