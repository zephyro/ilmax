<!doctype html>
<html lang="rus">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body id="top">
        <div class="wrap">

            <div class="wrap-content">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/nav.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-3 sidebar">

                                <!-- Sidenav -->
                                <?php include('inc/sidenav.inc.php') ?>
                                <!-- -->

                                <div class="rubric">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Статьи</div>
                                            <a href="#">Все статьи</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric2.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Правильное утепление крыши в деревянном доме</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="rubric-after">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Совет</div>
                                            <a href="#">Все советы</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-9 main-content">

                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><a href="#">Каталог</a></li>
                                    <li><a href="#">Фуги для плитки</a></li>
                                    <li><span>ilmax artcolor mastic</span></li>
                                </ul>

                                <h1 class="main_heading">ilmax artcolor mastic Фуга новинка artcolor mastic ilmax mastic Фуга новинка artcolor Фуга новинка artcolor mastic</h1>

                                <div class="product">

                                    <div class="product__gallery">
                                        <div class="product__gallery_slider">
                                            <div class="product__gallery_item">
                                                <a href="images/product_image.png" data-fancybox="gallery">
                                                    <img src="images/product_image.png" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                            <div class="product__gallery_item">
                                                <a href="images/product_image.png" data-fancybox="gallery">
                                                    <img src="images/product_image.png" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                            <div class="product__gallery_item">
                                                <a href="images/product_image.png" data-fancybox="gallery">
                                                    <img src="images/product_image.png" class="img-fluid" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="product__info">

                                        <ul class="product__params">
                                            <li>
                                                <i>
                                                    <img src="img/goods/icon__width.svg" class="img-fluid" alt="">
                                                </i>
                                                <div class="product__params_text"><span>Ширина шва до 8 мм</span></div>
                                            </li>
                                            <li>
                                                <i>
                                                    <img src="img/goods/icon__guard.svg" class="img-fluid" alt="">
                                                </i>
                                                <div  class="product__params_text"><span>Защита от грибка и плесени</span></div>
                                            </li>
                                            <li>
                                                <i>
                                                    <img src="img/goods/icon__water.svg" class="img-fluid" alt="">
                                                </i>
                                                <div class="product__params_text"><span>Водостойкая и трещиностойкая</span></div>
                                            </li>
                                            <li>
                                                <i>
                                                    <img src="img/goods/icon__count.svg" class="img-fluid" alt="">
                                                </i>
                                                <div class="product__params_text"><span>Равномерный и устойчивый цвет</span></div>
                                            </li>
                                        </ul>

                                        <div class="product__weight">
                                            <div class="product__weight_title">Доступная фасовка:</div>
                                            <div class="product__weight_value">
                                                <strong>10</strong>
                                                <span>кг</span>
                                            </div>
                                            <div class="product__weight_value">
                                                <strong>15</strong>
                                                <span>кг</span>
                                            </div>
                                        </div>

                                        <ul class="product__button">
                                            <li>
                                                <a href="#" class="btn_compare">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 14 15" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__chart" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="btn_calc"></a>
                                            </li>
                                            <li>
                                                <a href="#" class="btn_purchase">
                                                    <i>
                                                        <svg class="ico-svg" viewBox="0 0 12 17" xmlns="http://www.w3.org/2000/svg">
                                                            <use xlink:href="img/sprite_icons.svg#icon__placemark" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                        </svg>
                                                    </i>
                                                    <span>Где купить</span>
                                                </a>
                                            </li>
                                        </ul>

                                    </div>

                                </div>

                                <div class="content">

                                    <div class="product_content">

                                        <div class="product__block">
                                            <h4>Описание</h4>
                                            <div class="product_text">
                                                Каким должен быть дизайн интерьера кухни? Прежде всего, комфортабельным, эргономичным, детально продуманным. Разработка интерьера на высоком уровне требует вливания значительных средств и временных ресурсов. Ремонт начинается с создания дизайнот руки или в специальной программе.
                                                <a class="product_readmore" href="#"><span>Показать больше</span></a>
                                            </div>
                                        </div>

                                        <div class="product__block">
                                            <h4>Описание</h4>
                                            <div class="product_text">
                                                Каким должен быть дизайн интерьера кухни? Прежде всего, комфортабельным, эргономичным, детально продуманным. Разработка интерьера на высоком уровне требует вливания значительных средств и временных ресурсов. Ремонт начинается с создания дизайнот руки или в специальной программе.
                                                <a class="product_readmore" href="#"><span>Показать больше</span></a>
                                            </div>
                                        </div>

                                        <div class="product__block">
                                            <h4>Описание</h4>
                                            <div class="product_text">
                                                Каким должен быть дизайн интерьера кухни? Прежде всего, комфортабельным, эргономичным, детально продуманным. Разработка интерьера на высоком уровне требует вливания значительных средств и временных ресурсов. Ремонт начинается с создания дизайнот руки или в специальной программе.
                                                <a class="product_readmore" href="#"><span>Показать больше</span></a>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="product_tabs tabs">

                                        <ul class="product_tabs__nav tabs_nav">
                                            <li class="active"><a href="#tab1">Характеристики</a></li>
                                            <li><a href="#tab2">Инструкция</a></li>
                                            <li><a href="#tab3">Документация</a></li>
                                            <li><a href="#tab4">Видео</a></li>
                                            <li><a href="#tab5">Объекты</a></li>
                                            <li><a href="#tab6">Отзывы</a></li>
                                            <li><a href="#tab7">FAQ</a></li>
                                        </ul>

                                        <div class="product_tabs__content">

                                            <div class="product_tabs__item tabs_item active" id="tab1">
                                                <h4>Расход материалов (компонент А+ компонент Б)</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <tr>
                                                            <th>Условия эксплуатации</th>
                                                            <th>Толщина наносимого слоя</th>
                                                            <th>Расход</th>
                                                        </tr>
                                                        <tr>
                                                            <td>Высокая влажность</td>
                                                            <td>2,0 мм (2 слоя)</td>
                                                            <td>около 2,4 кг/м2</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Вода без давления</td>
                                                            <td>2,5 мм (2 слоя)</td>
                                                            <td>около 3,0 кг/м2</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Вода более 1 метра</td>
                                                            <td>3,0 мм (3 слоя)</td>
                                                            <td>3,6 кг/м2</td>
                                                        </tr>
                                                    </table>
                                                </div>

                                                <h4>Технические характеристики</h4>
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <tr>
                                                            <td>Цвет</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>серый</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Время использования раствора, не более</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>1,5 ч</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Расход</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>1,6 кг/м2*1мм</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Дальнейшие отделочные работы, не ранее</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>3 суток</strong>/td>
                                                        </tr>
                                                        <tr>
                                                            <td>Пригодна к эксплуатации, не ранее</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>7 суток</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Адгезия, не менее</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>1,2 МПа</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Водонепроницаемость, не менее</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>1,2 МПа</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Водопоглощение при капилярном подсосе, не более</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>8 атм</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Морозостойкость, не менее</td>
                                                            <td class="text-right"><strong>40 шт</strong></td>
                                                            <td class="text-right"><strong>0,2 кг/м2</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Температура проведения работ</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>-30°С...+70°С</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Температура эксплуатации</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>+5°С...+25°С</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Срок хранения</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>12 месяцев</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Фасовка</td>
                                                            <td class="text-right color_red"><strong>Компонент А 24 кг</strong></td>
                                                            <td class="text-right color_red"><strong>Компонент Б 8 кг (8 л)</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Количество единиц на поддоне</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>40 шт.</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Вес поддона</td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right"><strong>320 кг</strong></td>
                                                        </tr>
                                                    </table>















                                                </div>

                                            </div>

                                            <div class="product_tabs__item tabs_item" id="tab2">

                                                <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.</p>
                                                <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских времен.</p>
                                                <h5>Список:</h5>
                                                <ul class="list_point">
                                                    <li>Первый элемент списка</li>
                                                    <li>Второй элемент списка</li>
                                                    <li>Третий элемент списка</li>
                                                    <li>Четвертый элемент списка</li>
                                                </ul>

                                                <img src="images/img_01.jpg" class="img-fluid" alt="">

                                            </div>

                                            <div class="product_tabs__item tabs_item" id="tab3">

                                                <ul class="file_list">
                                                    <li>
                                                        <a class="file_link" href="#">
                                                            <span>Ссылка на скачивание другого файла</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="file_link" href="#">
                                                            <span>Ссылка на скачивание другого файла</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="file_link" href="#">
                                                            <span>Ссылка на скачивание другого файла</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="file_link" href="#">
                                                            <span>Ссылка на скачивание другого файла</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="file_link" href="#">
                                                            <span>Ссылка на скачивание другого файла</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="file_link" href="#">
                                                            <span>Ссылка на скачивание другого файла</span>
                                                        </a>
                                                    </li>
                                                </ul>

                                            </div>

                                            <div class="product_tabs__item tabs_item" id="tab4">

                                                <div class="gallery">
                                                    <div class="gallery__item">
                                                        <a class="gallery__image" href="images/g_img__01.jpg" data-fancybox="gl1">
                                                            <img src="images/g_img__01.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <span>Гидрофобизатор ilmax AQUA PROTECT</span>
                                                    </div>
                                                    <div class="gallery__item">
                                                        <a class="gallery__image" href="images/g_img__02.jpg" data-fancybox="gl1">
                                                            <img src="images/g_img__02.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <span>Нивелировка пола гипсовым самонивелиром</span>
                                                    </div>
                                                    <div class="gallery__item">
                                                        <a class="gallery__image" href="images/g_img__03.jpg" data-fancybox="gl1">
                                                            <img src="images/g_img__03.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <span>Гидрофобизатор ilmax AQUA PROTECT</span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="product_tabs__item tabs_item" id="tab5">

                                                <div class="gallery">
                                                    <div class="gallery__item">
                                                        <a class="gallery__image" href="#">
                                                            <img src="images/o_img__01.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <span>Бизнес центр Rubin Plaza</span>
                                                        <a class="gallery__read" href="#">Подробнее</a>
                                                    </div>
                                                    <div class="gallery__item">
                                                        <a class="gallery__image" href="#">
                                                            <img src="images/o_img__02.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <span>Жилой комплекс "Вивальди"</span>
                                                        <a class="gallery__read" href="#">Подробнее</a>
                                                    </div>
                                                    <div class="gallery__item">
                                                        <a class="gallery__image" href="#">
                                                            <img src="images/o_img__03.jpg" class="img-fluid" alt="">
                                                        </a>
                                                        <span>Спортивный комплекс М. Мирного</span>
                                                        <a class="gallery__read" href="#">Подробнее</a>
                                                    </div>
                                                </div>
                                                <link rel="stylesheet" href="js/vendor/raty.js/jquery.raty.css">
                                            </div>

                                            <div class="product_tabs__item tabs_item" id="tab6">

                                                <div class="comment">
                                                    <div class="comment__body">
                                                        <div class="comment__heading">
                                                            <div class="comment__heading_text">ООО „Рога и копыта“</div>
                                                            <div class="comment__heading_rate">
                                                                <div class="raty" data-score="4.0" data-readOnly="true"></div>
                                                            </div>
                                                        </div>

                                                        <div class="comment__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div>
                                                    <div class="comment__image">
                                                        <a href="images/scan_01.jpg" data-fancybox="comment">
                                                            <img src="images/scan_01.jpg" class="img-fluid" alt="">
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="comment">
                                                    <div class="comment__body">
                                                        <div class="comment__heading">
                                                            <div class="comment__heading_text">ООО „Рога и копыта“</div>
                                                            <div class="comment__heading_rate">
                                                                <div class="raty" data-score="4.5" data-readOnly="true"></div>
                                                            </div>
                                                        </div>

                                                        <div class="comment__text">
                                                            <p>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия.</p>
                                                        </div>
                                                    </div>
                                                    <div class="comment__image">
                                                        <a href="images/scan_01.jpg" data-fancybox="comment">
                                                            <img src="images/scan_01.jpg" class="img-fluid" alt="">
                                                        </a>
                                                    </div>
                                                </div>

                                                <div class="comment_new">
                                                    <h5>Оставьте свой отзыв:</h5>
                                                    <form class="form">

                                                        <div class="comment_new__box">
                                                            <div class="form_group">
                                                                <label class="form_label">Имя или Наименование юридического лица <sup>*</sup></label>
                                                                <input type="text" class="form_control" name="" value="" placeholder="ООО „Рога и копыта“">
                                                            </div>
                                                            <div class="form_group">
                                                                <label class="form_label">Email <sup>*</sup></label>
                                                                <input type="text" class="form_control" name="" value="" placeholder="example@mail.com">
                                                            </div>
                                                            <div class="form_group">
                                                                <label class="form_label">Телефон <sup>*</sup></label>
                                                                <input type="text" class="form_control" name="" value="" placeholder="example@mail.com">
                                                            </div>
                                                            <div class="form_group">
                                                                <label class="form_label">Оценка (от 1 до 5) <sup>*</sup></label>
                                                                <div class="raty"></div>
                                                            </div>
                                                        </div>

                                                        <div class="form_group">
                                                            <label class="form_label">Текст отзыва <sup>*</sup></label>
                                                            <textarea class="form_control" name="" placeholder="" rows="8">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия
                                                            </textarea>
                                                        </div>

                                                        <div class="form_group">
                                                            <div class="file_form">
                                                                <label class="file_form__label">
                                                                    <input type="file" name="file" value="">
                                                                    <i>
                                                                        <svg class="ico-svg" viewBox="0 0 15 18" xmlns="http://www.w3.org/2000/svg">
                                                                            <use xlink:href="img/sprite_icons.svg#icon__clip" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                                        </svg>
                                                                    </i>
                                                                    <span>Прикрепить файл</span>
                                                                </label>
                                                                <div class="file_form__elem">
                                                                    <span>Название_файла.jpg</span>
                                                                    <i class="file_form__remove"></i>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form_group">
                                                            <button type="submit" class="btn_main">Отправить отзыв</button>
                                                        </div>

                                                    </form>
                                                </div>

                                            </div>

                                            <div class="product_tabs__item tabs_item" id="tab7">

                                                <div class="faq open">
                                                    <div class="faq__question">
                                                        <span>Как рассчитать количество стяжки для пола с большим перепадом высоты?</span>
                                                        <i>
                                                            <svg viewBox="0 0 10 5" xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__angle_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                    <div class="faq__answer">Воспользуйтесь нашим удобным <a href="#">калькулятором</a></div>
                                                </div>

                                                <div class="faq">
                                                    <div class="faq__question">
                                                        <span>Как рассчитать количество стяжки для пола с большим перепадом высоты?</span>
                                                        <i>
                                                            <svg viewBox="0 0 10 5" xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__angle_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                    <div class="faq__answer">Воспользуйтесь нашим удобным <a href="#">калькулятором</a></div>
                                                </div>

                                                <div class="faq">
                                                    <div class="faq__question">
                                                        <span>Подскажите пожалуйста какие материалы применить для внутренней отделки стен из газосиликатных блоков в дачном домике, который эксплуатируется в теплое время года</span>
                                                        <i>
                                                            <svg viewBox="0 0 10 5" xmlns="http://www.w3.org/2000/svg">
                                                                <use xlink:href="img/sprite_icons.svg#icon__angle_down" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                            </svg>
                                                        </i>
                                                    </div>
                                                    <div class="faq__answer">Воспользуйтесь нашим удобным <a href="#">калькулятором</a></div>
                                                </div>

                                                <div class="new_question">
                                                    <h5>Задайте свой вопрос:</h5>
                                                    <form class="form">
                                                        <div class="comment_new__box">
                                                            <div class="form_group">
                                                                <label class="form_label">Имя или Наименование юридического лица <sup>*</sup></label>
                                                                <input type="text" class="form_control" name="" value="" placeholder="ООО „Рога и копыта“">
                                                            </div>
                                                            <div class="form_group">
                                                                <label class="form_label">Email <sup>*</sup></label>
                                                                <input type="text" class="form_control" name="" value="" placeholder="example@mail.com">
                                                            </div>
                                                            <div class="form_group">
                                                                <label class="form_label">Телефон <sup>*</sup></label>
                                                                <input type="text" class="form_control" name="" value="" placeholder="example@mail.com">
                                                            </div>
                                                        </div>
                                                        <div class="form_group">
                                                            <label class="form_label">Текст отзыва <sup>*</sup></label>
                                                            <textarea class="form_control" name="" placeholder="" rows="8">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым для визуально-слухового восприятия
                                                            </textarea>
                                                        </div>
                                                        <div class="form_group">
                                                            <button type="submit" class="btn_main">Отправить вопрос</button>
                                                        </div>
                                                    </form>
                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="product__block">
                                        Каким должен быть дизайн интерьера кухни? Прежде всего, комфортабельным, эргономичным, детально продуманным. Разработка интерьера на высоком уровне требует вливания значительных средств и временных ресурсов. Ремонт начинается с создания дизайнот руки или в специальной программе.
                                    </div>

                                </div>

                                <div class="slide_box">
                                    <div class="section-head">
                                        <div class="h3">Рекомендованные</div>
                                        <div class="section-control">
                                            <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="slider_border new-ones-slider">
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section-control section-control-mobile">
                                        <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                        <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>

                                <div class="slide_box">
                                    <div class="section-head">
                                        <div class="h3">Похожие</div>
                                        <div class="section-control">
                                            <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="slider_border new-ones-slider">
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="goods">
                                                <div class="goods__image">
                                                    <img src="img/new-ones.png" class="img-fluid">
                                                </div>
                                                <div class="goods__content">

                                                    <div class="goods__data">
                                                        <div class="goods__data_name">
                                                            <span>ilmax 2000</span>
                                                            <strong>ФУГА ЭЛАСТИЧНАЯ</strong>
                                                        </div>
                                                        <div class="goods__data_type">
                                                            МИНЕРАЛЬНАЯ
                                                            <br/>
                                                            ВЫРАВНИВАЮЩАЯ
                                                        </div>
                                                        <div class="goods__data_text">ilmax 3000 предназначен для приклеивания керамической плитки размером до 40х40 см</div>
                                                        <ul class="goods__data_info">
                                                            <li>Подходит для пола и стен</li>
                                                            <li>Для керамической плитки</li>
                                                            <li>Для плитки размером до 40х40 см</li>
                                                            <li>Подходит для влажных работ</li>
                                                        </ul>
                                                    </div>

                                                    <div class="goods__action">
                                                        <a href="#" class="goods__button goods__button_compare">
                                                            <span>Сравнить</span>
                                                        </a>
                                                        <a href="#" class="goods__button goods__button_view">
                                                            <span>Подробнее</span>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="section-control section-control-mobile">
                                        <button class="sect-contr-prev prev-new-ones"><i class="fas fa-chevron-left"></i></button>
                                        <button class="sect-contr-next next-new-ones"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>

                                <!-- News -->
                                <div class="news">
                                    <div class="section-head">
                                        <div class="h3">Новости
                                            <span><a href="#">Все новости</a></span>
                                        </div>
                                        <div class="section-control">
                                            <button class="sect-contr-prev prev-news"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-news"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                    <div class="slider-wrapper">
                                        <div class="news-slider">
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news1.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news2.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news3.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news1.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news2.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                            <div>
                                                <a class="news-item" href="#">
                                                    <img src="img/news/news3.jpg" alt="">
                                                    <div class="news-item-text">
                                                        <p class="news-topic">Новинка! Готовая к применению шпатлевка ilmax ready coat</p>
                                                        <p class="date-news">11 октября 2018</p>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="section-control section-control-mobile">
                                            <button class="sect-contr-prev prev-news"><i class="fas fa-chevron-left"></i></button>
                                            <button class="sect-contr-next next-news"><i class="fas fa-chevron-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <!-- -->

                            </div>

                        </div>
                    </div>

                    <!-- Feedback -->
                    <?php include('inc/feedback.inc.php') ?>
                    <!-- -->

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

      </body>

</html>
