<header class="pt-2">
    <nav class="navbar navbar-expand-lg general-nav">
        <div class="container">
            <a href="" class="navbar-brand">
                <img src="img/logo.png" alt="">
            </a>
            <div class="mobile-main-btn ml-auto">
                <button type="button" class="mx-1  func-btn">
                    <img src="img/calc.svg" class="calc" alt="">
                </button>
                <button type="button" class="mx-1 func-btn func-btn-zero">
                    <img src="img/stat.svg" class="calc" alt="">
                    <span class="calc-zero">0</span>
                </button>
            </div>

            <button class="navbar-toggler c-hamburger c-hamburger--htx menu_btn mx-lg-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <form class="form-inline input-group my-lg-2 my-lg-0 mx-auto">
                    <input type="text" class="form-control  search-pr" placeholder="Поиск по товарам" aria-label="Поиск по товарам">
                    <div class="input-group-append">
                        <button class="btn loupe" type="button" id="button-addon2">
                            <img src="img/loup.svg" alt="">
                        </button>
                    </div>
                </form>
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item"><a href="#" class="nav-link">О компании</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">Где купить</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">Объекты</a></li>
                    <li class="nav-item"><a href="#" class="nav-link">Контакты</a></li>
                </ul>

                <section class="main-nav main-nav-mobile">
                    <ul class="nav nav-pills nav-fill">
                        <li class="nav-item catalog-item justify-content-between">
                            <a href="#" class="nav-link" data-toggle="collapse" data-target="#hide-mobile-sidebar">Каталог<span><img src="img/catalog-arrow.svg" alt=""></span></a>

                            <div class="sidebar collapse" id="hide-mobile-sidebar">
                                <ul class="nav flex-column">
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Новинки</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Клеи для плитки</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Фуги для плитки</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Клеи для гипсокартона</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Клеи для блоков</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Кладочные смеси</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Штукатурки выравнивающие</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Шпатлевки</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Стяжки и самонивелиры</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Грунтовки</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Гидро- и пароизоляция</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Клеи для систем утепления</a></li>
                                    <li class="nav-item nav-border"><a href="#" class="nav-link">Защитно-отделочные штукатурки</a></li>
                                    <li class="nav-item restore">
                                        <div class="menu-addit">
                                            <p class="menu-addit-text text-center">restore</p>
                                        </div>
                                        <a href="#" class="nav-link">Реставрационные смеси</a>
                                    </li>
                                    <li class="nav-item industry">
                                        <div class="menu-addit">
                                            <p class="menu-addit-text text-center">industry</p>
                                        </div>
                                        <a href="#" class="nav-link">Составы повышенной прочности</a>
                                    </li>
                                    <li class="nav-item turbo">
                                        <div class="menu-addit">
                                            <p class="menu-addit-text text-center">turbo</p>
                                        </div>
                                        <a href="" class="nav-link">Смеси машинного нанесения</a>
                                    </li>
                                    <li class="nav-item thermo">
                                        <div class="menu-addit">
                                            <p class="menu-addit-text text-center">thermo</p>
                                        </div>
                                        <a href="#" class="nav-link">Теплосберегающие смеси</a>
                                    </li>
                                    <li class="nav-item protect">
                                        <div class="menu-addit">
                                            <p class="menu-addit-text text-center">protect</p>
                                        </div>
                                        <a href="#" class="nav-link">Защитные составы</a></li>
                                    <li class="nav-item ecobud">
                                        <div class="menu-addit">
                                            <p class="menu-addit-text text-center">экабуд</p>
                                        </div>
                                        <a href="#" class="nav-link">Сухие строительные смеси</a>
                                    </li>
                                    <li class="nav-item ecobud">
                                        <div class="menu-addit">
                                            <p class="menu-addit-text text-center">экабуд</p>
                                        </div>
                                        <a href="#" class="nav-link">Экабуд</a>
                                    </li>
                                    <li class="nav-item ready-coat">
                                        <div class="menu-addit">
                                            <p class="menu-addit-text text-center">ready coat</p>
                                        </div>
                                        <a href="" class="nav-link">Готовые к применению составы</a>
                                    </li>
                                </ul>
                            </div>

                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link ">документация</a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link">советы</a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link">статьи</a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link">акции</a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link">видео</a>
                        </li>
                        <li class="nav-item">
                            <a href="" class="nav-link">вопрос-ответ</a>
                        </li>
                    </ul>
                </section>
                <form class="form-inline my-2 my-lg-0 mx-xl-2  ml-auto nav-btn-group">
                    <button type="button" class="mx-xl-2 mx-lg-1 func-btn">
                        <img src="img/calc.svg" class="calc" alt="">
                    </button>
                    <button type="button" class="mx-xl-2 mx-lg-1 func-btn func-btn-zero">
                        <img src="img/stat.svg" class="calc" alt="">
                        <div class="calc-zero">0</div>
                    </button>
                    <button type="button" class="mx-xl-2 mx-lg-1 func-btn-phone dropdown-toggle" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="img/phone.svg" class="calc" alt="">
                    </button>
                    <div class="ml-auto my-2 my-lg-0 collapse phone-hide dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                        <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                    </div>
                </form>
                <div class=" my-2 my-lg-0 phone-main">
                    <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                    <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                </div>
            </div>

        </div>
    </nav>
    <div class="fixed-top">
        <nav class="navbar navbar-expand-lg nav-scroll">
            <div class="container">
                <a href="" class="navbar-brand">
                    <img src="img/logo-small.png" alt="">
                </a>
                <div class="collapse navbar-collapse">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle catalog-btn" data-toggle="collapse" data-target="#hide">
                            <span>Каталог</span>
                            <span class="down-catalog"><i class="fas fa-chevron-down"></i></span>
                            <span class="cross"> <i class="fas fa-times"></i></span>
                        </button>
                    </div>
                    <div class="dropdown">
                        <button class="nav__showmenu  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            ☰
                        </button>
                        <div class="dropdown-menu burger-drop" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Новости</a>
                            <a class="dropdown-item" href="#">Вакансии</a>
                            <a class="dropdown-item" href="#">Сотрудничество</a>
                            <a class="dropdown-item" href="#">Отзывы</a>
                            <a class="dropdown-item" href="#">Награды</a>
                        </div>
                    </div>

                    <form class="form-inline input-group my-2 mx-auto  my-lg-0 ">
                        <input type="text" class="form-control  search-pr" placeholder="Поиск по товарам" aria-label="Поиск по товарам">
                        <div class="input-group-append">
                            <button class="btn loupe" type="button" id="button-addon2"><img src="img/loup.svg" alt=""></button>
                        </div>
                    </form>
                    <form class="form-inline my-2 mx-xl-2 mx-lg-0 my-lg-0 ml-auto">
                        <button type="button" class="mx-2 func-btn">
                            <img src="img/calc.svg" class="calc" alt="">
                        </button>
                        <button type="button" class="mx-2 func-btn func-btn-zero">
                            <img src="img/stat.svg" class="calc" alt="">
                            <div class="calc-zero">0</div>
                        </button>
                        <button type="button" class="mx-xl-2 mx-lg-1 func-btn-phone dropdown-toggle" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="img/phone.svg" class="calc" alt="">
                        </button>
                        <div class="ml-auto my-2 my-lg-0 collapse phone-hide dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                            <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                        </div>
                    </form>
                    <div class=" my-2 my-lg-0 phone-number phone-main">
                        <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                        <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                    </div>
                </div>
            </div>
        </nav>
        <div class="collapse catalog-fix" id="hide">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <ul>
                            <li>
                                <a class="catalog-fix-link" href="">Новинки</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Клеи для плитки</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Фуги для плитки</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Клеи для гипсокартона</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Клеи для блоков</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Кладочные смеси</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul>
                            <li>
                                <a class="catalog-fix-link" href="">Штукатурки выравнивающие</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Шпатлевки</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Стяжки и самонивелиры</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Грунтовки</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Гидро- и пароизоляция</a>
                            </li>
                            <li>
                                <a class="catalog-fix-link" href="">Клеи для систем утепления</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul>
                            <li>
                                <a class="catalog-fix-link" href="">Защитно-отделочные штукатурки</a>
                            </li>
                            <li>
                                <p class="link-fix-add restore-fix">restore</p>
                                <a class="catalog-fix-link" href="">Реставрационные смеси</a>
                            </li>
                            <li>
                                <p class="link-fix-add industry-fix">industry</p>
                                <a class="catalog-fix-link" href="">Составы повышенной </a>
                            </li>
                            <li>
                                <p class="link-fix-add turbo-fix">turbo</p>
                                <a class="catalog-fix-link" href="">Смеси машинного нанесения</a>
                            </li>
                            <li>
                                <p class="link-fix-add thermo-fix">thermo</p>
                                <a class="catalog-fix-link" href="">Теплосберегающие смеси</a>
                            </li>
                            <li>
                                <p class="link-fix-add protect-fix">protect</p>
                                <a class="catalog-fix-link" href="">Защитные составы</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <ul>
                            <li>
                                <p class="link-fix-add ecobud-fix">экабуд</p>
                                <a class="catalog-fix-link" href="">Сухие строительные смеси</a>
                            </li>
                            <li>
                                <p class="link-fix-add ecobud-fix">экабуд</p>
                                <a class="catalog-fix-link " href="">Экабуд</a>
                            </li>
                            <li>
                                <p class="link-fix-add ready-fix">ready coat</p>
                                <a class="catalog-fix-link" href="">Готовые к применению составы</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>