<section class="main-nav">
    <div class="container">
        <div class="">
            <ul class="nav nav-pills nav-fill">
                <li class="nav-item col-md-3 catalog-item justify-content-between">
                    <a href="" class="nav-link">Каталог</a>
                </li>
                <li class="nav-item doc-drop">
                    <a href="" class="nav-link">документация</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">советы</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">статьи</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">акции</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">видео</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link">вопрос-ответ</a>
                </li>
            </ul>
        </div>
    </div>
</section>