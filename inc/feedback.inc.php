<div class="feedback" style="background-image: url('img/feedback-back.png');">
    <img src="img/man.png" class="man" alt="">
    <div class="container-fluid">
        <div class="feedback-content">
            <div class="row justify-content-between">
                <div class="col-4 question-mobile">
                    <div class="h2">Задайте вопрос специалисту</div>
                </div>
                <div class="col-sm-5">
                    <div class="h4">Ивановский Иван</div>
                    <p>Технический специалист</p>
                    <div class="row btn-entry">
                        <div class="col-xl-6 col-xs-7 btn ques-bt">
                            <a href="#" class="study">Записаться на обучение</a>
                        </div>
                        <div class="col-xl-6 col-xs-7  btn ques-bt">
                            <a href="#" class="question">Задать вопрос</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>