<ul class="nav flex-column">
    <li class="nav-item nav-border"><a href="#" class="nav-link">Новинки</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Клеи для плитки</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Фуги для плитки</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Клеи для гипсокартона</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Клеи для блоков</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Кладочные смеси</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Штукатурки выравнивающие</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Шпатлевки</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Стяжки и самонивелиры</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Грунтовки</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Гидро- и пароизоляция</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Клеи для систем утепления</a></li>
    <li class="nav-item nav-border"><a href="#" class="nav-link">Защитно-отделочные штукатурки</a></li>
    <li class="nav-item restore">
        <div class="menu-addit">
            <p class="menu-addit-text text-center">restore</p>
        </div>
        <a href="#" class="nav-link">Реставрационные смеси</a>
    </li>
    <li class="nav-item industry">
        <div class="menu-addit">
            <p class="menu-addit-text text-center">industry</p>
        </div>
        <a href="#" class="nav-link">Составы повышенной прочности</a>
    </li>
    <li class="nav-item turbo">
        <div class="menu-addit">
            <p class="menu-addit-text text-center">turbo</p>
        </div>
        <a href="#" class="nav-link">Смеси машинного нанесения</a>
    </li>
    <li class="nav-item thermo">
        <div class="menu-addit">
            <p class="menu-addit-text text-center">thermo</p>
        </div>
        <a href="#" class="nav-link">Теплосберегающие смеси</a>
    </li>
    <li class="nav-item protect">
        <div class="menu-addit">
            <p class="menu-addit-text text-center">protect</p>
        </div>
        <a href="#" class="nav-link">Защитные составы</a>
    </li>
    <li class="nav-item ecobud">
        <div class="menu-addit">
            <p class="menu-addit-text text-center">экабуд</p></div>
        <a href="#" class="nav-link">Сухие строительные смеси</a>
    </li>
    <li class="nav-item ecobud">
        <div class="menu-addit">
            <p class="menu-addit-text text-center">экабуд</p>
        </div>
        <a href="#" class="nav-link">Экабуд</a>
    </li>
    <li class="nav-item ready-coat">
        <div class="menu-addit">
            <p class="menu-addit-text text-center">ready coat</p>
        </div>
        <a href="#" class="nav-link">Готовые к применению составы</a>
    </li>
</ul>