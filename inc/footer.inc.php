<footer>
    <div class="footer-nav">
        <ul class="nav nav-pills nav-fill">
            <li class="nav-item"><a href="#" class="nav-link">Где купить</a></li>
            <li class="nav-item"><a href="#" class="nav-link">каталог</a></li>
            <li class="nav-item"><a href="#" class="nav-link">статьи</a></li>
            <li class="nav-item"><a href="#" class="nav-link">видео</a></li>
            <li class="nav-item"><a href="#" class="nav-link">объекты</a></li>
            <li class="nav-item"><a href="#" class="nav-link">контакты</a></li>
        </ul>
    </div>
    <nav class="navbar navbar-expand-lg">
        <div class="container">
            <a href="" class="navbar-brand">
                <img src="img/logo-small.png" alt ="">
            </a>
            <div class="collapse navbar-collapse">
                <form class="form-inline input-group my-2 my-lg-0 ">
                    <input type="text" class="form-control  search-pr" placeholder="Поиск по товарам" aria-label="Поиск по товарам">
                    <div class="input-group-append">
                        <button class="btn loupe" type="button" id="button-addon2">
                            <img src="img/loup.svg" alt="">
                        </button>
                    </div>
                </form>

                <div class="social mx-auto">
                    <a class="mx-2 social-link"><span class="calc"><i class="fab fa-youtube"></i></span></a>
                    <a class="mx-2 social-link"><span class="calc"><i class="fab fa-instagram"></i></span></a>
                    <a class="mx-2 social-link"><span class="calc"><i class="fab fa-facebook-f"></i></span></a>
                    <a class="mx-2 social-link"><span class="calc"><i class="fab fa-vk"></i></span></a>
                </div>
            </div>

            <form class="form-inline my-2 my-lg-0 ml-auto">
                <button type="button" class="mx-2 func-btn-phone">
                    <img src="img/phone.svg" class="calc" alt="">
                </button>
            </form>

            <div class=" my-2 my-lg-0 phone-footer">
                <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
                <a href="tel:+375172890069" class="phone-header">(017) 289-00-69</a>
            </div>
        </div>
    </nav>

    <div class="footer-rights">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col">2018 © Все права защищены.</div>
                <div class="col click">Сделано в <a href="https://clickmedia.by/">Clickmedia</a></div>
            </div>
        </div>
    </div>

</footer>

<a href="#top" class="btn_up btn_scroll"><i class="fas fa-caret-up"></i></a>