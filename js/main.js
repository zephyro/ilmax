$(document).ready(function () {

    $('.btn_scroll').click(function(){
        var str=$(this).attr('href');
        $.scrollTo(str, 500, {offset: 0});
        return false;
    });

// SVG IE11 support
    svg4everybody();


    $('.select_single__label').on('click touchstart', function(e) {
        e.preventDefault();

        if($(this).closest('.select_main').hasClass('active')){
            $(this).closest('.select_main').toggleClass('active');
        }
        else {
            $('.select_main').removeClass('active');
            $(this).closest('.select_main').toggleClass('active');
        }
    });

    $('.select_multiple__label').on('click touchstart', function(e) {
        e.preventDefault();

        if($(this).closest('.select_main').hasClass('active')){
            $(this).closest('.select_main').toggleClass('active');
        }
        else {
            $('.select_main').removeClass('active');
            $(this).closest('.select_main').toggleClass('active');
        }
    });

    $('.select_multiple__close').on('click touchstart', function(e) {
        e.preventDefault();
        $(this).closest('.select_main').removeClass('active');
    });


    $('.select_single input[type="radio"]').change(function(e) {
        var box = $(this).closest('.select_single');
        var check = box.find('input[type="radio"]:checked');
        box.removeClass('active');
        var txt = check.val();
        box.find('.select_single__label span').text(txt);
    });

    $('.select_multiple input[type="checkbox"]').change(function(e) {
        var stat = $(this).prop('checked');
        var box = $(this).closest('.select_multiple');
        var amount = parseInt(box.find('.select_multiple__label_value').text());
        if (stat) {
            amount = amount + 1;
        }
        else  {
            amount = amount - 1
        }
        box.find('.select_multiple__label_value').text(amount);
    });

    // hide dropdown

    $('body').click(function (event) {

        if ($(event.target).closest(".select_main").length === 0) {
            $(".select_main").removeClass('active');
        }
    });

    $('.main_filter__view a').on('click touchstart', function(e) {
        e.preventDefault();
        var status = $(this).hasClass('active');

        if (status) {

        }
        else {
            $(this).closest('.main_filter__view').find('a').removeClass('active');
            $(this).addClass('active');
            $('.showcase').toggleClass('showcase_list');
            $('.showcase').find('.goods').toggleClass('goods_list');
        }
    });

    // Product Gallery

    $('.product__gallery_slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        arrows: false
    });

    //Rating {

    $('.raty').raty({
        number: 5,
        starHalf      : 'fas fa-star-half-alt',
        starOff       : 'fas  fa-star star-off',
        starOn        : 'fas fa-star',
        cancelOff     : 'far fa-minus-square',
        cancelOn      : 'far fa-plus-square',
        score: function() {
            return $(this).attr('data-score');
        },
        readOnly: function() {
            return $(this).attr('data-readOnly');
        },
    });

    // form file

    $('.file_form input[type="file"]').on('change', function(e) {
        var box = $(this).closest('.file_form');
        var str = $(this).val();

        if (str.lastIndexOf('\\')){
            var i = str.lastIndexOf('\\')+1;
        }
        else{
            var i = str.lastIndexOf('/')+1;
        }
        var filename = str.slice(i);
        box.find('.file_form__elem span').text(filename);
        box.addClass('selected');
    });

    $('.file_form__remove').on('click touchstart', function(e) {
        e.preventDefault();
        var box = $(this).closest('.file_form');
        box.removeClass('selected');
        box.find('.file_form__elem span').text('');
    });


    $('.faq__question').on('click touchstart', function(e) {
        e.preventDefault();
        $(this).closest('.faq').toggleClass('open');
    });

    /*


    var count = $('.compare__row:first-child > .compare__elem_data').length;
    var dataWidth = $(".compare__elem_data").width();

    count = count - 2;

    var boxWidth = $(".compare").width() + dataWidth * count;
    console.log(count);
    console.log(dataWidth);
    console.log(boxWidth);

    $('.compare__wrap').width(boxWidth);
*/

});

(function($){
    $(window).on("load",function(){

        var wh = $(window).width();

        if (wh > 767) {

            $('.compare__scroll').mCustomScrollbar({
                axis:"x",
                theme:"dark",
                mouseWheel:{scrollAmount:205,normalizeDelta:true},
                snapAmount:205,
                scrollButtons:{enable:true,scrollType:"stepped"},
            });

        }
    });

    $(window).resize(function() {

        var wh = $(window).width();
        var status = $(".compare__scroll").hasClass("mCustomScrollbar");

        console.log(status);

        if (wh < 768) {
            $('.compare__scroll').mCustomScrollbar("destroy");
        }
        else {
            if (!status) {

                $('.compare__scroll').mCustomScrollbar({
                    axis:"x",
                    theme:"dark",
                    mouseWheel:{scrollAmount:205,normalizeDelta:true},
                    snapAmount:205,
                    scrollButtons:{enable:true,scrollType:"stepped"},
                });
            }
            else {
                $('.compare__scroll').mCustomScrollbar("update");
            }
        }
    });

})(jQuery);




//scroll up
$(window).on("scroll", function() {
    if($(window).scrollTop() > 1000) {
        $(".btn_up").addClass("btn_up__visible");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
        $(".btn_up").removeClass("btn_up__visible");
    }
});


$(".btn_modal").fancybox({
    'padding'    : 0
});



// Tabs

(function() {

    $('.tabs_nav > li > a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("href"));
        var box = $(this).closest('.tabs');

        $(this).closest('.tabs_nav').find('li').removeClass('active');
        $(this).closest('li').addClass('active');

        box.find('.tabs_item').removeClass('active');
        box.find(tab).addClass('active');
    });

}());
