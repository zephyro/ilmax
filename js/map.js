ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [53.901806109028854,27.54669611111444],
            zoom: 8,
            controls: ['zoomControl']
        }),

        clusterer = new ymaps.Clusterer({
            preset: 'islands#invertedDarkBlueClusterIcons',
            clusterHideIconOnBalloonOpen: false,
            geoObjectHideIconOnBalloonOpen: false
        }),

        getPointData = [

            // Минск
            {
                balloonContentBody: "<div class='map_points'>" +
                    "<a class='map_points__name' href='#'>Название компании</a>" +
                    "<div class='map_points__address'>Адрес: г. Минск, Партизанский пр., д. 6а</div>" +
                    "<div class='map_points__metro'>Партизанская</div>" +
                    "<ul class='map_points__phones'>" +
                    "<li><a href='tel:+7 (800) 200 00 00'>+7 (800) 200 00 00</a></li>" +
                    "<li><a href='tel:+7 (449) 334 56 92'>+7 (449) 334 56 92</a></li>" +
                    "<li><a href='tel:+7 (448) 852 02 02'>+7 (448) 852 02 02</a>" +
                    "</li>" +
                    "</ul>" +
                    "<ul class='map_points__email'>" +
                    "<li><a href='mailto:info@site.ru'>info@site.ru</a></li>" +
                    "<li><a href='mailto:first-site.ru'>first-site.ru</a></li>" +
                    "<li><a href='mailto:second-site.ru'>second-site.ru</a></li>" +
                    "</ul>",
                clusterCaption: "<div class='map_points__cord'>N 48.521016’  E 48.521016’</div>"
            },
            {
                balloonContentBody: "<div class='map_points'>" +
                    "<a class='map_points__name' href='#'>Название компании</a>" +
                    "<div class='map_points__address'>Адрес: г. Минск, ул. Толстого, 10</div>" +
                    "<div class='map_points__metro'>Иститут Культуры</div>" +
                    "<ul class='map_points__phones'>" +
                    "<li><a href='tel:+7 (800) 200 00 00'>+7 (800) 200 00 00</a></li>" +
                    "<li><a href='tel:+7 (449) 334 56 92'>+7 (449) 334 56 92</a></li>" +
                    "<li><a href='tel:+7 (448) 852 02 02'>+7 (448) 852 02 02</a>" +
                    "</li>" +
                    "</ul>" +
                    "<ul class='map_points__email'>" +
                    "<li><a href='mailto:info@site.ru'>info@site.ru</a></li>" +
                    "<li><a href='mailto:first-site.ru'>first-site.ru</a></li>" +
                    "<li><a href='mailto:second-site.ru'>second-site.ru</a></li>" +
                    "</ul>",
                clusterCaption: "<div class='map_points__cord'>N 48.521016’  E 48.521016’</div>"
            },
            {
                balloonContentBody: "<div class='map_points'>" +
                    "<a class='map_points__name' href='#'>Название компании</a>" +
                    "<div class='map_points__address'>Адрес: г. Минск, Победителей пр., д. 9</div>" +
                    "<div class='map_points__metro'>Партизанская</div>" +
                    "<ul class='map_points__phones'>" +
                    "<li><a href='tel:+7 (800) 200 00 00'>+7 (800) 200 00 00</a></li>" +
                    "<li><a href='tel:+7 (449) 334 56 92'>+7 (449) 334 56 92</a></li>" +
                    "<li><a href='tel:+7 (448) 852 02 02'>+7 (448) 852 02 02</a>" +
                    "</li>" +
                    "</ul>" +
                    "<ul class='map_points__email'>" +
                    "<li><a href='mailto:info@site.ru'>info@site.ru</a></li>" +
                    "<li><a href='mailto:first-site.ru'>first-site.ru</a></li>" +
                    "<li><a href='mailto:second-site.ru'>second-site.ru</a></li>" +
                    "</ul>",
                clusterCaption: "<div class='map_points__cord'>N 48.521016’  E 48.521016’</div>"
            },

            // Витебск
            {
                balloonContentBody: "<div class='map_points'>" +
                    "<a class='map_points__name' href='#'>Название компании</a>" +
                    "<div class='map_points__address'>Адрес: г. Витебск, проспект Фрунзе, 27</div>" +
                    "<ul class='map_points__phones'>" +
                    "<li><a href='tel:+7 (800) 200 00 00'>+7 (800) 200 00 00</a></li>" +
                    "<li><a href='tel:+7 (449) 334 56 92'>+7 (449) 334 56 92</a></li>" +
                    "<li><a href='tel:+7 (448) 852 02 02'>+7 (448) 852 02 02</a>" +
                    "</li>" +
                    "</ul>" +
                    "<ul class='map_points__email'>" +
                    "<li><a href='mailto:info@site.ru'>info@site.ru</a></li>" +
                    "<li><a href='mailto:first-site.ru'>first-site.ru</a></li>" +
                    "<li><a href='mailto:second-site.ru'>second-site.ru</a></li>" +
                    "</ul>",
                clusterCaption: "<div class='map_points__cord'>N 48.521016’  E 48.521016’</div>"
            },
            {
                balloonContentBody: "<div class='map_points'>" +
                    "<a class='map_points__name' href='#'>Название компании</a>" +
                    "<div class='map_points__address'>Адрес: г. Витебск, Максима Горького, 145 </div>" +
                    "<ul class='map_points__phones'>" +
                    "<li><a href='tel:+7 (800) 200 00 00'>+7 (800) 200 00 00</a></li>" +
                    "<li><a href='tel:+7 (449) 334 56 92'>+7 (449) 334 56 92</a></li>" +
                    "<li><a href='tel:+7 (448) 852 02 02'>+7 (448) 852 02 02</a>" +
                    "</li>" +
                    "</ul>" +
                    "<ul class='map_points__email'>" +
                    "<li><a href='mailto:info@site.ru'>info@site.ru</a></li>" +
                    "<li><a href='mailto:first-site.ru'>first-site.ru</a></li>" +
                    "<li><a href='mailto:second-site.ru'>second-site.ru</a></li>" +
                    "</ul>",
                clusterCaption: "<div class='map_points__cord'>N 48.521016’  E 48.521016’</div>"
            },
            {
                balloonContentBody: "<div class='map_points'>" +
                    "<a class='map_points__name' href='#'>Название компании</a>" +
                    "<div class='map_points__address'>Адрес: г. Витебск, Московский проспект, 8 </div>" +
                    "<ul class='map_points__phones'>" +
                    "<li><a href='tel:+7 (800) 200 00 00'>+7 (800) 200 00 00</a></li>" +
                    "<li><a href='tel:+7 (449) 334 56 92'>+7 (449) 334 56 92</a></li>" +
                    "<li><a href='tel:+7 (448) 852 02 02'>+7 (448) 852 02 02</a>" +
                    "</li>" +
                    "</ul>" +
                    "<ul class='map_points__email'>" +
                    "<li><a href='mailto:info@site.ru'>info@site.ru</a></li>" +
                    "<li><a href='mailto:first-site.ru'>first-site.ru</a></li>" +
                    "<li><a href='mailto:second-site.ru'>second-site.ru</a></li>" +
                    "</ul>",
                clusterCaption: "<div class='map_points__cord'>N 48.521016’  E 48.521016’</div>"
            },

            // Гродно
            {
                balloonContentBody: "<div class='map_points'>" +
                    "<a class='map_points__name' href='#'>Название компании</a>" +
                    "<div class='map_points__address'>Адрес: г. Гродно, Магистральная улица, 22 </div>" +
                    "<ul class='map_points__phones'>" +
                    "<li><a href='tel:+7 (800) 200 00 00'>+7 (800) 200 00 00</a></li>" +
                    "<li><a href='tel:+7 (449) 334 56 92'>+7 (449) 334 56 92</a></li>" +
                    "<li><a href='tel:+7 (448) 852 02 02'>+7 (448) 852 02 02</a>" +
                    "</li>" +
                    "</ul>" +
                    "<ul class='map_points__email'>" +
                    "<li><a href='mailto:info@site.ru'>info@site.ru</a></li>" +
                    "<li><a href='mailto:first-site.ru'>first-site.ru</a></li>" +
                    "<li><a href='mailto:second-site.ru'>second-site.ru</a></li>" +
                    "</ul>",
                clusterCaption: "<div class='map_points__cord'>N 48.521016’  E 48.521016’</div>"
            },
            {
                balloonContentBody: "<div class='map_points'>" +
                    "<a class='map_points__name' href='#'>Название компании</a>" +
                    "<div class='map_points__address'>Адрес: г. Гродно, Весенняя улица, 19А</div>" +
                    "<ul class='map_points__phones'>" +
                    "<li><a href='tel:+7 (800) 200 00 00'>+7 (800) 200 00 00</a></li>" +
                    "<li><a href='tel:+7 (449) 334 56 92'>+7 (449) 334 56 92</a></li>" +
                    "<li><a href='tel:+7 (448) 852 02 02'>+7 (448) 852 02 02</a>" +
                    "</li>" +
                    "</ul>" +
                    "<ul class='map_points__email'>" +
                    "<li><a href='mailto:info@site.ru'>info@site.ru</a></li>" +
                    "<li><a href='mailto:first-site.ru'>first-site.ru</a></li>" +
                    "<li><a href='mailto:second-site.ru'>second-site.ru</a></li>" +
                    "</ul>",
                clusterCaption: "<div class='map_points__cord'>N 48.521016’  E 48.521016’</div>"
            },
        ],

        getPointOptions = [

            // Минск
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/placemark_01.png',
                iconImageSize: [28, 28],
                iconImageOffset: [-14, -14]
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/placemark_02.png',
                iconImageSize: [28, 28],
                iconImageOffset: [-14, -14]
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/placemark_03.png',
                iconImageSize: [28, 28],
                iconImageOffset: [-14, -14]
            },

            // Витебск
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/placemark_01.png',
                iconImageSize: [28, 28],
                iconImageOffset: [-14, -14]
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/placemark_02.png',
                iconImageSize: [28, 28],
                iconImageOffset: [-14, -14]
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/placemark_03.png',
                iconImageSize: [28, 28],
                iconImageOffset: [-14, -14]
            },

            // Гродно
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/placemark_01.png',
                iconImageSize: [28, 28],
                iconImageOffset: [-14, -14]
            },
            {
                iconLayout: 'default#image',
                iconImageHref: 'img/placemark_02.png',
                iconImageSize: [28, 28],
                iconImageOffset: [-14, -14]
            },

        ],

        points = [
            [53.88440757068166,27.58358850000002],
            [53.88832957069177,27.54429599999998],
            [53.90855407065631,27.548598999999946],

            [55.192651569519796,30.219164499999945],
            [55.17009506955063,30.12891949999995],
            [55.183395069525595,30.206246499999985],

            [53.6877210708457,23.855965999999995],
            [53.68685207084346,23.79997399999997],

        ],
        geoObjects = [];

    for(var i = 0, len = points.length; i < len; i++) {
        geoObjects[i] = new ymaps.Placemark(points[i], getPointData[i], getPointOptions[i]);
    }



    clusterer.add(geoObjects);
    myMap.geoObjects.add(clusterer);

    myMap.setBounds(clusterer.getBounds(), {
        checkZoomRange: true
    });

    myMap.behaviors.disable('scrollZoom');
});
