// button catalog
$('.catalog-btn').click(function(){
	$(this).toggleClass('catalog-btn-act');
	$('.down-catalog').toggleClass('down-catalog-d-n');
	$('.cross').toggleClass('cross-d-in-b');
})
$('.active-drop').click(function(){
	$(this).toggleClass('options-btn-act');
	$('.down1').toggleClass('down-chevron-d-n');
	$('.up1').toggleClass('chevron-up-d-in');
})
$('.active-drop1').click(function(){
	$(this).toggleClass('options-btn-act');
	$('.down2').toggleClass('down-chevron-d-n'); 
	$('.up2').toggleClass('chevron-up-d-in');
})
$('.active-drop2').click(function(){
	$(this).toggleClass('options-btn-act');
	$('.down3').toggleClass('down-chevron-d-n');
	$('.up3').toggleClass('chevron-up-d-in');
})
$('.active-drop3').click(function(){
	$(this).toggleClass('options-btn-act');
	$('.down4').toggleClass('down-chevron-d-n');
	$('.up4').toggleClass('chevron-up-d-in');
})
$('.active-drop4').click(function(){
	$(this).toggleClass('options-btn-act');
	$('.down5').toggleClass('down-chevron-d-n');
	$('.up5').toggleClass('chevron-up-d-in');
})
$('.close2').click(function(){
	$('.down2').toggleClass('down-chevron-d-n');
	$('.up2').toggleClass('chevron-up-d-in');
	$('.active-drop1').toggleClass('options-btn-act');
})
$('.close3').click(function(){
	$('.down3').toggleClass('down-chevron-d-n');
	$('.up3').toggleClass('chevron-up-d-in');
	$('.active-drop2').toggleClass('options-btn-act');
})
$('.close4').click(function(){
	$('.down4').toggleClass('down-chevron-d-n');
	$('.up4').toggleClass('chevron-up-d-in');
	$('.active-drop3').toggleClass('options-btn-act');
})
$('.close5').click(function(){
	$('.down5').toggleClass('down-chevron-d-n');
	$('.up5').toggleClass('chevron-up-d-in');
	$('.active-drop4').toggleClass('options-btn-act');
})
//phone number hide
function checkPosition() {
    if (window.matchMedia('(max-width: 1200px)').matches) {
        $('.').addClass()
    } else {
       $(".").removeClass()
    }
}
//burger script
		(function() {

		  "use strict";
		 
		  var toggles = document.querySelectorAll(".c-hamburger");
		 
		  for (var i = toggles.length - 1; i >= 0; i--) {
		    var toggle = toggles[i];
		    toggleHandler(toggle);
		  };
		 
		  function toggleHandler(toggle) {
		    toggle.addEventListener( "click", function(e) {
		      e.preventDefault();
		      (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
		    });
		  }
		 
		})();
//main-slider
 $('.single-item').slick({
            prevArrow: $('.prev'),
            nextArrow: $('.next')
          });
//news-slider
 $('.news-slider').slick({
            prevArrow: $('.prev-news'),
            nextArrow: $('.next-news'),
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                  breakpoint:1199,
                  settings:{
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
              },
                {
                  breakpoint:767,
                  settings:{
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
              }  
            ]
          });
//video-slider
$('.video-slider').slick({
    prevArrow: $('.prev-video'),
    nextArrow: $('.next-video'),
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
          breakpoint:1199,
          settings:{
            slidesToShow: 2,
            slidesToScroll: 2
        }
      },
        {
          breakpoint:767,
          settings:{
            slidesToShow: 1,
            slidesToScroll: 1
        }
      }  
    ]
  });
//new product slider
$('.new-ones-slider').slick({
            prevArrow: $('.prev-new-ones'),
            nextArrow: $('.next-new-ones'),
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                  breakpoint:1199,
                  settings:{
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
              },
                {
                  breakpoint:767,
                  settings:{
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
              }  
            ]
          });
//low scroll
$(window).on("scroll", function() {
        if($(window).scrollTop() > 100) {
            $(".nav-scroll").addClass("nav-scroll-active");
        } else {
            //remove the background property so it comes transparent again (defined in your css)
           $(".nav-scroll").removeClass("nav-scroll-active");
        }
    });





















