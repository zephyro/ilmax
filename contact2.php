<!doctype html>
<html lang="rus">

    <!-- Head -->
    <?php include('inc/head.inc.php') ?>
    <!-- -->

    <body id="top">
        <div class="wrap">

            <div class="wrap-content">

                <!-- Header -->
                <?php include('inc/header.inc.php') ?>
                <!-- -->

                <!-- Navigation -->
                <?php include('inc/nav.inc.php') ?>
                <!-- -->

                <section class="main">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-3 sidebar">

                                <div class="contact_nav">
                                    <div class="contact_nav__title">Выбор региона</div>
                                    <ul>
                                        <li class="active"><a href="#">Минская область</a></li>
                                        <li><a href="#">Витебская область</a></li>
                                        <li><a href="#">Гродненская область</a></li>
                                        <li><a href="#">Могилевская область</a></li>
                                        <li><a href="#">Брестская область</a></li>
                                        <li><a href="#">Гомельская область</a></li>
                                    </ul>
                                </div>


                                <div class="rubric">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Статьи</div>
                                            <a href="#">Все статьи</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric2.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Правильное утепление крыши в деревянном доме</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="rubric-after">
                                    <div class="rubric-main">
                                        <div class="rubric-head justify-content-between align-items-center">
                                            <div class="h4">Совет</div>
                                            <a href="#">Все советы</a>
                                        </div>
                                        <div class="rubric-content align-items-center">
                                            <div class="rubric-logo">
                                                <a href="#">
                                                    <img src="img/rubric1.png" alt="">
                                                </a>
                                            </div>
                                            <p class="rubric-descr">
                                                <a href="#">Как не превратить ремонт дома в кошмар?</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-9 main-content">

                                <ul class="breadcrumbs">
                                    <li><a href="#">Главная</a></li>
                                    <li><span>Контакты</span></li>
                                </ul>

                                <h1>Контакты</h1>

                                <div class="contact_map" id="map"></div>

                                <div class="contact_row">

                                    <div class="contact_elem">
                                        <div class="contact_elem__title">Приемная:</div>
                                        <p><a class="contact_elem__phone" href="tel:+375 (17) 289-00-68">+375 (17) 289-00-68</a></p>
                                        <p><a class="contact_elem__phone" href="tel:+375 (44) 555-12-3">+375 (44) 555-12-30</a></p>
                                        <p><a class="contact_elem__email" href="mailto:office@ilmax.by">office@ilmax.by</a></p>
                                    </div>

                                    <div class="contact_elem">
                                        <div class="contact_elem__title">Отдел продаж:</div>
                                        <p><a class="contact_elem__phone" href="tel:+375 (17) 289-00-68">+375 (17) 289-00-68</a></p>
                                        <p><a class="contact_elem__phone" href="tel:+375 (44) 555-12-3">+375 (44) 555-12-30</a></p>
                                        <p><a class="contact_elem__email" href="mailto:office@ilmax.by">office@ilmax.by</a></p>
                                    </div>

                                    <div class="contact_elem">
                                        <div class="contact_elem__title">Отдел маркетинга:</div>
                                        <p><a class="contact_elem__phone" href="tel:+375 (17) 289-00-68">+375 (17) 289-00-68</a></p>
                                        <p><a class="contact_elem__phone" href="tel:+375 (44) 555-12-3">+375 (44) 555-12-30</a></p>
                                        <p><a class="contact_elem__email" href="mailto:office@ilmax.by">office@ilmax.by</a></p>
                                    </div>

                                    <div class="contact_elem">
                                        <div class="contact_elem__title">ехнический центр:</div>
                                        <p><a class="contact_elem__phone" href="tel:+375 (17) 289-00-68">+375 (17) 289-00-68</a></p>
                                        <p><a class="contact_elem__phone" href="tel:+375 (44) 555-12-3">+375 (44) 555-12-30</a></p>
                                        <p><a class="contact_elem__email" href="mailto:office@ilmax.by">office@ilmax.by</a></p>
                                    </div>

                                </div>

                                <div class="contact__data mb_60">
                                    <p>
                                        ООО "Илмакс", УНН 100070995<br/>
                                        Республика Беларусь, 223050, Минский район,<br/>
                                        а/г Колодищи, yл. Хуторская, 1/3, а/я 35<br/>
                                        р/с BY09PJCB30120151321000000933 в ЦБУ № 118<br/>
                                        ОАО «Приорбанк», BIK (SWIFT) PJCBBY2X,<br/>
                                        г. Минск, ул. Сурганова, д.41
                                    </p>
                                     
                                    <p>
                                        Время работы<br/>
                                        пн-пт: 8.30 - 17.00
                                    </p>
                                     
                                    <p>Карту проезда на склад в а.п. Колодищи, ул. Хуторская,1 можно скачать <a href="#">здесь.</p>
                                </div>


                            </div>

                        </div>
                    </div>

                    <!-- Feedback -->
                    <?php include('inc/feedback.inc.php') ?>
                    <!-- -->

                </section>

                <!-- Footer -->
                <?php include('inc/footer.inc.php') ?>
                <!-- -->

            </div>

        </div>

        <!-- Scripts -->
        <?php include('inc/scripts.inc.php') ?>
        <!-- -->

        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

        <script src="js/map.js" type="text/javascript"></script>

      </body>

</html>
